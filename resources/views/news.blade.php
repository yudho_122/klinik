@extends('admintemplate.layout')

@section('content')
<br>
<div class="row">
    <div class="col s12">
        <div class="page-title">Master Data</div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <span class="card-title">News Data</span>
                <!-- <p>This is the news list that has been written all the time</p><br> -->
                <button class="waves-effect waves-light btn blue m-b-xs modal-trigger" href="#modal1"><i class="material-icons left">add_box</i>Tambah</button>
                <table id="example" class="display responsive-table datatable-example">
                    <thead>
                        <tr>
                            <th>News Title</th>
                            <th>News Content</th>
                            <th>Created at</th>
                            <th>Created by</th>
                            <th>Operation</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                          <th>News Title</th>
                          <th>News Content</th>
                          <th>Created at</th>
                          <th>Created by</th>
                          <th>Operation</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <tr>
                            <td>Tiger Nixon</td>
                            <td>System Architect</td>
                            <td>Edinburgh</td>
                            <td>$320,800</td>
                            <td>

                              <button class="btn indigo" style="bottom: 10px; right: 10px;"><i class="material-icons">info</i></button>
                              <button class="btn blue" style="bottom: 10px; right: 10px;"><i class="material-icons">mode_edit</i></button>
                              <button class="btn red" style="bottom: 10px; right: 10px;"><i class="material-icons">delete</i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Create New News</h4>
            <div class="input-field col s12">
              <input id="title" type="text" class="validate" length="255">
              <label for="password" class="">Title</label>
            </div>
            <div class="input-field col s12">
              <form action="#" class="p-v-xs">
                <div class="file-field input-field">
                  <div class="btn white lighten-1">
                    <span>File</span>
                    <input type="file">
                  </div>
                  <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                  </div>
                </div>
              </form>
            </div>
            <div class="input-field col s12">
              <textarea id="textarea1" class="materialize-textarea"></textarea>
              <label for="textarea1">Textarea</label>
            </div>
            <button class="waves-effect waves-light btn blue m-b-xs">Save</button>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Close</a>
        </div>
    </div>
</div>
@endsection
