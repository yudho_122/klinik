@extends('admintemplate.layout')

@section('content')
<br>

<div class="row" ng-app="newsRecords" ng-controller="newssController">
    <div class="col s12">
        <div class="page-title">Master Data</div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <span class="card-title">News Data</span>
                <!-- <p>This is the news list that has been written all the time</p><br> -->
                <button class="waves-effect waves-light btn blue m-b-xs modal-trigger" href="#modal1" ng-click="toggle('add', 0)"><i class="material-icons left">add_box</i>Tambah</button>
                <div class="display responsive-table datatable-example">
                 <table id="example" datatable="ng" class="table" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                        <th>News Title</th>
                        <th>News Content</th>
                        <th>Created at</th>
                        <th>Created by</th>
                        <th>Operation</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                      <th>News Title</th>
                      <th>News Content</th>
                      <th>Created at</th>
                      <th>Created by</th>
                      <th>Operation</th>
                    </tr>
                </tfoot>
                <tbody>
                    <tr ng-repeat="newz in news" id="text-tr">
                        <td>@{{ newz.news_title }}</td>
                        <td>@{{ newz.news_content }}</td>
                        <td>@{{ newz.created_at }}</td>
                        <td>@{{ newz.created_by }}</td>
                        <td style="width: 30%;">
                          <a class="btn indigo" style="bottom: 10px; right: 100px;" href="{{ url('/api/v1/news/view') }}/@{{ newz.news_id }}"><i class="material-icons">info</i></a>
                          <button id="modal-trigger-@{{ newz.news_id }}" class="waves-effect waves-light btn blue m-b-xs modal-trigger" style="bottom: 10px; left: 0.5px; top: 5px;" href="#modal@{{ newz.news_id }}" ng-click="toggle('edit', newz.news_id)" ng-mouseover="toggle('init', newz.news_id)"><i class="material-icons">mode_edit</i></button>
                          <button class="btn red" style="bottom: 10px; left: 200px;" ng-click="confirmDelete(newz.news_id)"><i class="material-icons">delete</i></button>

                          <script type="text/javascript">
                            //$('button[class*="modal-trigger"]').leanModal();
                          </script>

                          <!-- edit modal -->
                          <div id="modal@{{ newz.news_id }}" class="modal">
                              <div class="modal-content">
                                  <h4>@{{form_title}}</h4>

                                    {{ csrf_field() }}
                                  <div class="input-field col s12">
                                    <input id="news_title" class="validate news_title" name="news_title" type="text" length="255"
                                    ng-model="news.news_title" value="@{{newz.news_title}}">
                                    <label for="news_title">Title</label>
                                  </div>

                                  <div class="input-field col s12">
                                      <div class="file-field input-field">
                                        <div class="btn white lighten-1">
                                          <span>File</span>
                                          <input type="file" class="validate" ng-files="setTheFiles($files)" id="news_picture_path" >
                                          </div>
                                        <div class="file-path-wrapper">
                                          <input class="file-path validate" type="text" value="@{{newz.news_picture_path}}">
                                        </div>
                                      </div>
                                  </div>

                                  <div class="input-field col s12">
                                    <textarea id="news_content" name="news_content" class="materialize-textarea" ng-model="news.news_content"
                                    value="@{{newz.news_content}}"></textarea>
                                    <label for="news_content">News Content</label>
                                  </div>

                                  <div class="input-field col s12">
                                    <input id="created_by" name="created_by" type="text" class="validate" length="255"
                                    ng-model="news.created_by" value="@{{newz.created_by}}">
                                    <label for="created_by" class="">Created By</label>
                                  </div>
                                </div>

                              <div class="modal-footer">
                                  <button type="button" class="modal-acion modal-close waves-effect waves-blue btn-flat" id="btn-save" ng-click="edit(modalstate, newz.news_id, news.news_title, news.news_content, news.created_by)">Save</button>
                                  <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">cancel</a>
                              </div>
                          </div>
                          <!-- end edit modal -->
                        </td>
                    </tr>
                </tbody>
              </table>
              </div>
            </div>
        </div>
    </div>
    <!-- ini modal buat create -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4>@{{form_title}}</h4>
            <form>
              {{ csrf_field() }}
            <div class="input-field col s12">
              <input id="news_title" class="validate" name="news_title" type="text" length="255"
              ng-model="news.news_title">
              <label for="news_title">Title</label>
            </div>

            <div class="input-field col s12">
                <div class="file-field input-field">
                  <div class="btn white lighten-1">
                    <span>File</span>
                    <input type="file" class="validate" ng-files="setTheFiles($files)" id="news_picture_path">
                    </div>
                  <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                  </div>
                </div>
            </div>

            <div class="input-field col s12">
              <textarea id="news_content" name="news_content" class="materialize-textarea" ng-model="news.news_content"></textarea>
              <label for="news_content">News Content</label>
            </div>

            <div class="input-field col s12">
              <input id="created_by" name="created_by" type="text" class="validate" length="255"
              ng-model="news.created_by">
              <label class="">Created By</label>
            </div>
          </div>
      </form>
        <div class="modal-footer">
            <button type="button" class="modal-acion modal-close waves-effect waves-blue btn-flat" id="btn-save" ng-click="save(modalstate, news.news_id, news.news_title, news.news_content, news.created_by)">Save</button>
            <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">cancel</a>
        </div>
    </div>
</div>

@endsection

@section('js')
<script>

</script>
@endsection
