var app = angular.module('newsRecords', ['datatables'])
        .constant('API_URL', 'http://localhost/klinik-hutan/public/api/v1/');

app.controller('newssController', function($scope, $timeout ,$http, $window, API_URL) {
    //retrieve employees listing from API
    $scope.files = [];
    $scope.errors = [];
    $window.location;

    $http.get(API_URL + "news")
            .success(function(response) {
                $scope.news = response;
            });

    //show modal form
    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.news = "";
                $scope.form_title = "Add New News";
                break;
            case 'init':
              $('#modal-trigger-' + id).leanModal();
            break;
            case 'edit':
              $scope.form_title = "News Detail";
              $scope.news_id = id;

              $http.get(API_URL + 'news/' + id)
                      .success(function(response) {
                          console.log(response);

                          //-- set form to focus
                          $('label[for*="news_title"], label[for*="news_content"], label[for*="created_by"]').addClass( 'active' );

                          $scope.news.news_title = response.news_title;
                          $scope.news.news_content = response.news_content;
                          $scope.news.created_by = response.created_by;
                      });

                break;
            default:
                break;
        }

    }

    //-- instansiasi object formData
    var formData = new FormData();

    $scope.setTheFiles = function ($files) {
        angular.forEach($files, function (value, key) {
            formData.append('news_picture_path', value);
        });
    };


    $scope.edit = function(modalstate, id, news_title, news_content, created_by){
        var url = API_URL + "news/" + id;

        formData.append('news_title', news_title);
        formData.append('news_content', news_content);
        formData.append('created_by', created_by);

        $http({
            method: 'POST',
            url: url,
            data: formData,
            headers: {'Content-Type': undefined},
        }).success(function(response) {
            console.log(response);
            location.reload();
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    };

    //save new record / update existing record
    $scope.save = function(modalstate, id, news_title, news_content, created_by) {
        var url = API_URL + "news";

        //append employee id to the URL if the form is in edit mode
        if (modalstate === 'edit' && modalstate === 'init'){
            url += "/" + id;
        }

        formData.append('news_title', news_title);
        formData.append('news_content', news_content);
        formData.append('created_by', created_by);

        $http({
            method: 'POST',
            url: url,
            data: formData,
            headers: {'Content-Type': undefined},
        }).success(function(response) {
            console.log(response);
            location.reload();
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

    //redirect ke /news alias news_index
    $scope.redirectNews = function() {
          $window.location.href = '/news';
    }

    //delete record
    $scope.confirmDelete = function(id) {
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'news/' + id
            }).
                    success(function(data) {
                        console.log(data);
                        //location.reload();
                        $window.location.href = '/news';
                        //$window.location.href;

                    }).
                    error(function(data) {
                        console.log(data);
                        alert('Unable to delete');
                    });
        } else {
            return false;
        }

    }
});

app.directive('ngFiles', ['$parse', function ($parse) {

    function file_links(scope, element, attrs) {
        var onChange = $parse(attrs.ngFiles);
        element.on('change', function (event) {
            onChange(scope, {$files: event.target.files});
        });
    }

    return {
        link: file_links
    }
}]);
