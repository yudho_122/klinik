@extends('admintemplate.layout')

@section('content')
<br>

<div class="row" ng-app="resourceRecords" ng-controller="resourcesController">
    <div class="col s12">
        <div class="page-title">Master Data</div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <span class="card-title">Resource Data</span>
                <!-- <p>This is the news list that has been written all the time</p><br> -->
                <button class="waves-effect waves-light btn blue m-b-xs modal-trigger" href="#modal1" ng-click="toggle('add', 0)"><i class="material-icons left">add_box</i>Tambah</button>
                <table id="example" datatable="ng" class="display responsive-table datatable-example">
                    <thead>
                        <tr>
                            <th>Resource Title</th>
                            <th>Resource Desciption</th>
                            <th>Created at</th>
                            <th>Created by</th>
                            <th>Operation</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                          <th>Resource Title</th>
                          <th>Resource Desciption</th>
                          <th>Created at</th>
                          <th>Created by</th>
                          <th>Operation</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <tr ng-repeat="re in resource" id="text-tr">
                            <td>@{{ re.resource_title }}</td>
                            <td>@{{ re.resource_description }}</td>
                            <td>@{{ re.created_at }}</td>
                            <td>@{{ re.created_by }}</td>
                            <td style="width: 30%;">
                              <a class="btn indigo" style="bottom: 10px; right: 100px;" href="{{ url('/api/v1/resource/view') }}/@{{ re.resource_id }}"><i class="material-icons">info</i></a>
                              <button id="modal-trigger-@{{ re.resource_id }}" class="waves-effect waves-light btn blue m-b-xs modal-trigger" style="bottom: 10px; left: 0.5px; top: 5px;" href="#modal@{{ re.resource_id }}" ng-click="toggle('edit', re.resource_id)" ng-mouseover="toggle('init', re.resource_id)"><i class="material-icons">mode_edit</i></button>
                              <button class="btn red" style="bottom: 10px; left: 200px;" ng-click="confirmDelete(re.resource_id)"><i class="material-icons">delete</i></button>

                              <script type="text/javascript">
                                //$('button[class*="modal-trigger"]').leanModal();
                              </script>

                              <!-- edit modal -->
                              <div id="modal@{{ re.resource_id }}" class="modal">
                                  <div class="modal-content">
                                      <h4>@{{form_title}}</h4>

                                        {{ csrf_field() }}
                                      <div class="input-field col s12">
                                        <input id="resource_title" class="validate resource_title" name="resource_title" type="text" length="255"
                                        ng-model="resource.resource_title" value="@{{re.resource_title}}">
                                        <label for="resource_title">Resource Title</label>
                                      </div>

                                      <div class="input-field col s12">
                                          <div class="file-field input-field">
                                            <div class="btn white lighten-1">
                                              <span>File</span>
                                              <input type="file" class="validate" ng-files="setTheFiles($files)" id="resource_path">
                                              </div>
                                            <div class="file-path-wrapper">
                                              <input class="file-path validate" type="text" value="@{{re.resource_path}}">
                                            </div>
                                          </div>
                                      </div>

                                      <div class="input-field col s12">
                                        <textarea id="resource_description" name="resource_description" class="materialize-textarea" ng-model="resource.resource_description"
                                        value="@{{re.resource_description}}"></textarea>
                                        <label for="resource_description">Resource Description</label>
                                      </div>

                                      <div class="input-field col s12">
                                        <input id="created_by" class="validate created_by" name="created_by" type="text" length="45"
                                        ng-model="resource.created_by" value="@{{re.created_by}}">
                                        <label for="created_by" class="">Created By</label>
                                      </div>

                                  </div>

                                  <div class="modal-footer">
                                      <button type="button" class="modal-action modal-close waves-effect waves-blue btn-flat" id="btn-save" ng-click="edit(modalstate, re.resource_id, resource.resource_title, resource.resource_description, resource.created_by)">Save</button>
                                      <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">cancel</a>
                                  </div>
                              </div>
                              <!-- end edit modal -->
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- ini modal buat create -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4>@{{form_title}}</h4>
            <form>
              {{ csrf_field() }}
            <div class="input-field col s12">
              <input id="resource_title" class="validate" name="resource_title" type="text" length="255"
              ng-model="resource.resource_title">
              <label for="resource_title">Title</label>
            </div>

            <div class="input-field col s12">
                <div class="file-field input-field">
                  <div class="btn white lighten-1">
                    <span>File</span>
                    <input type="file" class="validate" ng-files="setTheFiles($files)" id="resource_path">
                    </div>
                  <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                  </div>
                </div>
            </div>

            <div class="input-field col s12">
              <textarea id="resource_description" name="resource_description" class="materialize-textarea" ng-model="resource.resource_description"></textarea>
              <label for="resource_description">Resource Description</label>
            </div>

            <div class="input-field col s12">
              <input id="created_by" name="created_by" type="text" class="validate" length="255"
              ng-model="resource.created_by">
              <label class="">Created by</label>
            </div>

        </div>
      </form>
        <div class="modal-footer">
            <button type="button" class="modal-action modal-close waves-effect waves-blue btn-flat" id="btn-save" ng-click="save(modalstate, resource.resource_id, resource.resource_title, resource.resource_description, resource.created_by)">Save</button>
            <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">cancel</a>
        </div>
    </div>
</div>

@endsection
