@extends('admintemplate.layout')

@section('content')
<div class="row">
    <div class="col s12">
        <div class="page-title">Report Page</div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
              <form method="POST">
              <span class="card-title">Pick a Date</span>
              <div class="row">
                  <div class="col s4">
                      <label for="birthdate">Start Date</label>
                      <input id="dateAwal" name="dateAwal" value="Tanggal Awal" type="text" class="datepicker">
                  </div>
                  <div class="col s4">
                      <label for="birthdate">End Date</label>
                      <input id="dateAkhir" name="dateAkhir" value="Tanggal Akhir" type="text" class="datepicker">
                  </div>
                  <div class="col s2">
                      <!-- <td><a href="#" class="waves-effect waves-light btn m-b-xs" style="top: 20px; left: 50px;">Process</a> -->
                    <button type="Submit" href="{{route('reportResolution')}}" name="submit" value="noPDF" class="waves-effect waves-light btn m-b-xs" style="top: 20px; left: 50px;">Submit</button>
                  </div>

              </div>
              <!-- <button type="button" name="button" id="getPdf">Save PDF</button> -->
            </form>
            <button type="button" id="getPdf" name="button" class="waves-effect waves-light btn red m-b-xs " style="float: right;">Save as PDF</button>
            <form method="GET" action="{{route('reportResolution')}}">
            <span class="card-title">Report from <b id="awal">{{ $start }}</b> until <b id="akhir">{{ $end }}</b></span>
            <table class="striped">
                <thead>
                    <tr>
                        <th>Subject</th>
                        <th>Category</th>
                        <th>Created At</th>
                        <th>Created By</th>
                        <th>Resolution Answer</th>
                      </tr>
                </thead>
                <tbody>
                  @foreach ($report as $repor)
                    <tr>
                        <td>{{$repor->subject}}</td>
                        <td>{{$repor->clinic_category_name}}</td>
                        <td>{{$repor->created_at}}</td>
                        <td>{{$repor->created_by}}</td>
                        <td>{{$repor->resolution_answer}}</td>
                    </tr>
                    @endforeach
                    <tr>

                    </tr>
                  </tbody>
            </table>
            <br>
                  <br><br>
          </form>
        </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
    $("#getPdf").click(function() {
        var dateAwal = $("#awal").html();
        var dateAkhir = $("#akhir").html();
        $('.lean-overlay').remove();
        $.ajax({
            type : 'POST',
            url  : "{{ route('getPdf') }}",
            data : {'dateAwal' : dateAwal, 'dateAkhir' : dateAkhir},
            success: function(response) {
              console.log(response);
            }
        });
    });
</script>
@endsection
