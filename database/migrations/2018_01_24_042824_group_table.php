<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up(){
      Schema::create('groups', function (Blueprint $table){
          $table->string('group_id',45)->unique()->primary();
          $table->string('group_name', 45);
          $table->text('group_des')->nullable();
          $table->dateTime('created_at');
          $table->string('created_by', 45)->nullable();
          $table->integer('clinic_category_id')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *

     */
    public function down()
    {
      Schema::drop('groups');
    }
}
