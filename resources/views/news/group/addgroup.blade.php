@extends('admintemplate.layout')

@section('content')
<div class="row" ng-app="groupRecords" ng-controller="groupsController">
    <div class="col s12">
        <div class="page-title">Master Data</div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <span class="card-title">Group Data</span>
                <!-- <p>This is the news list that has been written all the time</p><br> -->
                <button class="waves-effect waves-light btn blue m-b-xs modal-trigger" href="#modal1" ng-click="toggle('add', 0)"><i class="material-icons left">add_box</i>Tambah</button>
                <table id="example" datatable="ng" class="display responsive-table datatable-example">
                  <thead>
                      <tr>
                          <th>ID</th>
                          <th>Group Name</th>
                          <th>Group Description</th>
                          <th>Create At</th>
                          <th>Created By</th>
                          <th></th>
                      </tr>
                  </thead>
                  <tfoot>
                      <tr>
                        <th>ID</th>
                        <th>Group Name</th>
                        <th>Group Description</th>
                        <th>Create At</th>
                        <th>Created By</th>
                        <th></th>
                      </tr>
                  </tfoot>
                  <tbody>
                      <tr ng-repeat="grou in group">
                          <td style="width: 15%;" >@{{ grou.group_id }}</td>
                          <td>@{{ grou.group_name }}</td>
                          <td style="width: 30%;">@{{ grou.group_des }}</td>
                          <td style="width: 10%;">@{{ grou.created_at }}</td>
                          <td style="width: 10%;">@{{ grou.created_by }}</td>
                          <td style="width: 20%;">
                              <button id="modal-trigger-@{{ grou.group_id }}" class="waves-effect waves-light btn blue m-b-xs modal-trigger" style="bottom: 10px; left: 0.5px; top: 5px;" href="#modal@{{ grou.group_id }}" ng-click="toggle('edit', grou.group_id)" ng-mouseover="toggle('init', grou.group_id)"><i class="material-icons">mode_edit</i></button>
                              <button class="btn red" style="bottom: 10px; left: 200px;" ng-click="confirmDelete(grou.group_id)"><i class="material-icons">delete</i></button>

                              <!-- edit modal -->
                              <div id="modal@{{ grou.group_id }}" class="modal">
                                  <div class="modal-content">
                                      <h4>@{{form_title}}</h4>

                                        {{ csrf_field() }}
                                      <div class="input-field col s12">
                                        <p>@{{grou.group_id}}</p>
                                        <label for="group_id">Group ID</label>
                                      </div>

                                      <div class="input-field col s12">
                                        <input id="group_name" name="group_name" type="text" class="validate" length="45"
                                        ng-model="group.group_name" value="@{{grou.group_name}}">
                                        <label for="group_name" class="">Group Name</label>
                                      </div>

                                      <div class="input-field col s12">
                                        <textarea id="group_des" name="group_des" class="materialize-textarea" ng-model="group.group_des"
                                        value="@{{grou.group_des}}"></textarea>
                                        <label for="group_des">Group Description</label>
                                      </div>

                                      <div class="input-field col s12">
                                        <input id="created_by" name="created_by" type="text" class="validate" length="45"
                                        ng-model="group.created_by" value="@{{grou.created_by}}">
                                        <label for="created_by" class="">Created By</label>
                                      </div>

                                  </div>

                                  <div class="modal-footer">
                                     <button type="button" class="modal-action modal-close waves-effect waves-blue btn-flat" id="btn-save" ng-click="edit(modalstate, grou.group_id, group.group_name, group.group_des, group.created_by)">Save</button>
                                      <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">cancel</a>
                                  </div>
                              </div>
                              <!-- end edit modal -->
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- ini modal buat create -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4>@{{form_title}}</h4>
            <form>
              {{ csrf_field() }}
            <div class="input-field col s12">
              <input id="group_id" class="validate" name="group_id" type="text" length="45"
              ng-model="group.group_id">
              <label for="group_id">ID Group</label>
            </div>

            <div class="input-field col s12">
              <input id="group_name" name="group_name" type="text" class="validate" length="45"
              ng-model="group.group_name">
              <label for="group_name" class="">Group Name</label>
            </div>

            <div class="input-field col s12">
              <textarea id="group_des" name="group_des" class="materialize-textarea" ng-model="group.group_des"></textarea>
              <label for="group_des">Group Description</label>
            </div>

            <div class="input-field col s12">
              <input id="created_by" name="created_by" type="text" class="validate" length="45"
              ng-model="group.created_by">
              <label class="">Created By</label>
            </div>

        </div>
      </form>
        <div class="modal-footer">
            <button type="button" class="modal-action modal-close waves-effect waves-blue btn-flat" id="btn-save" ng-click="save(modalstate, group.group_id)">Save</button>
            <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">cancel</a>
        </div>
    </div>
</div>

@endsection
