<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sistem_setting extends Model
{
      protected $table = 'sistem_settings';
      protected $primaryKey = 'system_setting_key';
      public $incrementing = false;
      //public $timestamps = false;

      //protected $fillable = ['system_setting_key', 'system_setting_value','updated_at','updated_by'];


}
