<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';
    protected $primaryKey = 'group_id';
    public $incrementing = false;


//    protected $fillable = array('group_id', 'group_name', 'group_des','created_at','created_by');

    function user(){
      return $this->hasMany('App\User','group_id');
    }

    function permission(){
      return $this->hasMany('App\Permission','group_id');
    }

}
