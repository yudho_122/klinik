@extends('admintemplate.layout')

@section('content')

	<div class="row" ng-app="resourceRecords" ng-controller="resourcesController">
		<div class="col s8">
			<h1>{{$resource->resource_title}}</h1>
			<p><img src="#" style="max-height:100px;max-width:100px;margin-top:10px;"></p>
			<p class="lead">{{$resource->resource_description}}</p>


		</div>

      <div class="col s4">
        <div class="card white">
          <dl class="card-content">
            <label>Url:</label>
              <p><a href="#">ini url Thread</a></p>
          </dl>
          <dl class="card-content">
            <label>Created At:</label>
            <p>{{$resource->created_at}}</p>
          </dl>

          <dl class="card-content">
            <label>Last Updated:</label>
            <p>{{$resource->updated_at}}</p>
          </dl>
          <dl class="card-content">
            <label>Created By:</label>
            <p>{{$resource->created_by}}</p>
          </dl>

        <hr>
          <div class="row">
            <div class="col s6">
              <button id="modal-trigger-1" class="waves-effect waves-light btn greeb m-b-xs modal-trigger" style="width: 130px;" href="#modal1" ng-click="toggle('edit', {{$resource->resource_id}})" ng-mouseover="toggle('init', re.resource_id)">Edit</button>
							<div id="modal1" class="modal">
									<div class="modal-content">
											<h4>@{{form_title}}</h4>

												{{ csrf_field() }}
											<div class="input-field col s12">
												<input id="resource_title" class="validate resource_title" name="resource_title" type="text" length="255"
												ng-model="resource.resource_title" value="@{{re.resource_title}}">
												<label for="resource_title">Resource Title</label>
											</div>

											<div class="input-field col s12">
													<div class="file-field input-field">
														<div class="btn white lighten-1">
															<span>File</span>
															<input type="file" class="validate" ng-files="setTheFiles($files)" id="news_picture_path">
															</div>
														<div class="file-path-wrapper">
															<input class="file-path validate" type="text">
														</div>
													</div>
											</div>

											<div class="input-field col s12">
												<textarea id="resource_description" name="resource_description" class="materialize-textarea" ng-model="resource.resource_description"
												value="@{{re.resource_description}}"></textarea>
												<label for="resource_description">Resource Description</label>
											</div>

											<div class="input-field col s12">
												<input id="created_by" name="created_by" type="text" class="validate" length="255"
												ng-model="resource.created_by" value="@{{re.created_by}}">
												<label for="created_by" class="">created_by</label>
											</div>

									</div>

									<div class="modal-footer">
											<button type="button" class="modal-action modal-close waves-effect waves-blue btn-flat" id="btn-save" ng-click="edit(modalstate, {{$resource->resource_id}}, resource.resource_title, resource.resource_description, resource.created_by)">Save</button>
											<a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat">cancel</a>
									</div>
							</div>
          </div>

          <div class="col s6">
            <!--<a href="#" class="waves-effect waves-light btn red m-b-xs" style="width: 150px;">Delete</a>-->
            <button class="waves-effect waves-light btn m-b-xs modal-trigger red" ng-click="confirmDelete({{$resource->resource_id}})" style="width: 130px;">Delete</button>
                <div class="modal" id="modal3">
                    <div class="modal-content">
                      <h4>Delete Group</h4>
                              <a>Apakah anda yakin untuk menghapus?</a>
                              <div class="input-field">
                                <!--  <input id="first_name" type="text" class="validate">-->
                                  <button class="waves-effect waves-light btn m-b-xs modal-trigger">Yes</button>
                                  <button class="waves-effect waves-light btn m-b-xs modal-trigger">No</button>
                              </div>
                    </div>
              </div>
          </div>
          <div class="col s12 m6 l8">
            <a href="#" class="waves-effect waves-light btn blue m-b-xs" ng-click="redirectIndex()" style="width: 285px;">See All News</a>
          </div>
        </div>
        <div class="row">

        </div>
      </div>
      </div>
    </div>

@endsection
