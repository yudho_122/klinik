@extends('admintemplate.layout')

@section('content')
<br>

<div class="row" ng-app="userRecords" ng-controller="userController">
    <div class="col s12">
        <div class="page-title">Master Data</div>
    </div>
    <div class="col s12 m12 l12">
       <form action="{{ route('user.edit'), $user->email }}" method="POST" class="form-horizontal">
        <div class="card">
            <div class="card-content">

                <span class="card-title">User</span>

                <!-- <p>This is the news list that has been written all the time</p><br> -->
                {{ csrf_field() }}
                <div class="input-field col s12">
                  <input id="email" name="email" type="text" class="validate" length="45"
                  value="{{$user->email}}" readonly>
                  <label for="last_name" class="">Last Name</label>
                </div>

              <div class="input-field col s12">
                <input id="first_name" name="first_name" type="text" class="validate" length="45" value="{{$user->first_name}}">
                <label for="first_name" class="">First Name</label>
              </div>

              <div class="input-field col s12">
                <input id="last_name" name="last_name" type="text" class="validate" length="45"
                value="{{$user->last_name}}">
                <label for="last_name" class="">Last Name</label>
              </div>

              <div class="input-field col s12">
                <input id="password" name="password" type="text" class="validate"
                value="{{$user->password}}">
                <label for="password" class="">Password</label>
              </div>

              <div class="input-field col s12">
                <input id="telp" name="telp" type="text" class="validate" length="20"
                 value="{{$user->telp}}">
                <label for="telp" class="">Telefon</label>
              </div>

              <div class="input-field col s12">
                <input id="gcm_id" name="gcm_id" type="text" class="validate"
                 value="{{$user->gcm_id}}">
                <label for="gcm_id" class="">GCM ID</label>
              </div>

              <div class="input-field col s12">
                <input id="group_id" name="group_id" type="text" class="validate"
                 value="{{$user->group_id}}">
                <label for="group_id" class="">group ID</label>
              </div>

              <div class="input-field col s12">
                <input id="id_number" name="id_number" type="text" class="validate"
                 value="{{$user->id_number}}">
                <label for="id_number" class="">ID Number</label>
              </div>

              <div class="input-field col s12">
                <input id="alamat" name="alamat" type="text" class="validate"
                 value="{{$user->alamat}}">
                <label for="alamat">Alamat</label>
              </div>
              <!-- <input type="submit" class="modal-acion modal-close waves-effect waves-blue btn-flat" value="Save"> -->
              <button type="submit" class="modal-acion modal-close waves-effect waves-blue btn-flat" id="btn-save">Save</button>
              <button type="button" class="modal-action modal-close waves-effect waves-blue btn-flat " ng-click="toIndex()">Cancel</button>
            </div>
        </div>
      </form>
    </div>
</div>

@endsection
