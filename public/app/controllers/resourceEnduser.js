var app = angular.module('resourcenyaRecords', ['datatables'])
        .constant('API_URL', 'http://localhost/klinik-hutan/public/api/v1/');

app.controller('resourcesdiController', function($scope, $timeout ,$http, $window, API_URL) {
    //retrieve employees listing from API
    $scope.files = [];
    $scope.errors = [];
    $window.location;

    $http.get(API_URL + "resourcelist")
            .success(function(response) {
                $scope.resources = response;
            });
});
