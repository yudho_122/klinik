<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResourceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('resources', function (Blueprint $table) {
          $table->increments('resource_id')->unsigned();
          $table->string('resource_title', 45);
          $table->text('resource_description')->nullable();
          $table->text('resource_path')->nullable();
          $table->dateTime('created_at');
          $table->string('created_by', 45)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('resources');
    }
}
