<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use PDF;

class ReportResolutionAnswerController extends Controller
{
    public function index() {

    }

    public function reportResolution(Request $request) {
      $submit = $request->input('submit');
      $start = $request->input('dateAwal');
      $end = $request->input('dateAkhir');
      $awal 	=  Carbon::parse($start)->format('Y-m-d');
      $akhir 	=	 Carbon::parse($end)->format('Y-m-d');
    	$report 	=	DB::table('thread_cases')
    						->join('clinic_categorys', 'thread_cases.clinic_category_id', '=', 'clinic_categorys.clinic_category_id')
    						->select('thread_cases.subject', 'clinic_categorys.clinic_category_name', 'thread_cases.created_by', 'thread_cases.created_at', 'thread_cases.resolution_answer' )
    						->whereBetween('thread_cases.created_at', [$awal, $akhir])
    						->get();

          return view('report.resolution.view_res', compact('report', 'start', 'end'));
          return response()->json(['hasil' => $report]);
    }

    public function getPdf(Request $request) {
      $start = $request->input('dateAwal');
      $end = $request->input('dateAkhir');
      $awal 	=  Carbon::parse($start)->format('Y-m-d');
      $akhir 	=	 Carbon::parse($end)->format('Y-m-d');
      $report 	=	DB::table('thread_cases')
    						->join('clinic_categorys', 'thread_cases.clinic_category_id', '=', 'clinic_categorys.clinic_category_id')
    						->select('thread_cases.subject', 'clinic_categorys.clinic_category_name', 'thread_cases.created_by', 'thread_cases.created_at', 'thread_cases.resolution_answer' )
    						->whereBetween('thread_cases.created_at', [$awal, $akhir])
    						->get();

      $pdf = PDF::loadView('report.resolution.pdf1', compact('report', 'start', 'end'));
      $pdf->setPaper('A4', 'landscape');
      return $pdf->download('Resolution-'.$start.'-'.$end.'-Report.pdf');
    }
}
