<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    // use Enums;

    protected $table = 'users';
    protected $primaryKey = 'email';
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'first_name', 'last_name', 'password', 'alamat', 'group_id', 'telp', 'gcm_id', 'user_approval_status', 'id_number'];

    protected $enumUser_approval_statues = ['WAITING','APPROVE','REJECTED'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    function group(){
      return $this->belongsTo('App\Group', 'group_id', 'group_id');
    }

    function user_token(){
        return $this->hasMany('App\User_token','email');
    }
}
