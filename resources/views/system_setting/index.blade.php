@extends('admintemplate.layout')

@section('content')
<br>

<div class="row" ng-app="sistemsetRecords" ng-controller="sistemsetsController">
    <div class="col s12">
        <div class="page-title">Master Data</div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <span class="card-title">System Setting Data</span>
                <!-- <p>This is the news list that has been written all the time</p><br> -->
                <button class="waves-effect waves-light btn blue m-b-xs modal-trigger" href="#modal1" ng-click="toggle('add', 0)"><i class="material-icons left">add_box</i>Tambah</button>
                <table id="example" datatable="ng" class="display responsive-table datatable-example">
                    <thead>
                        <tr>
                            <th>Sytem Setting Key</th>
                            <th>System Setting Value</th>
                            <th>Created at</th>
                            <th>Updated at</th>
                            <th>Updated by</th>
                            <th>Operation</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                          <th>Sytem Setting Key</th>
                          <th>System Setting Value</th>
                          <th>Created at</th>
                          <th>Updated at</th>
                          <th>Updated by</th>
                          <th>Operation</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <tr ng-repeat="sist in sistem" id="text-tr">
                            <td>@{{ sist.system_setting_key }}</td>
                            <td>@{{ sist.system_setting_value }}</td>
                            <td>@{{ sist.created_at }}</td>
                            <td>@{{ sist.updated_at }}</td>
                            <td>@{{ sist.updated_by }}</td>
                            <td style="width: 30%;">
                              <button id="modal-trigger-@{{ sist.system_setting_key }}" class="waves-effect waves-light btn blue m-b-xs modal-trigger" style="bottom: 10px; left: 0.5px; top: 5px;" href="#modal@{{ sist.system_setting_key }}" ng-click="toggle('edit', sist.system_setting_key)" ng-mouseover="toggle('init', sist.system_setting_key)"><i class="material-icons">mode_edit</i></button>
                              <button class="btn red" style="bottom: 10px; left: 200px;" ng-click="confirmDelete(sist.system_setting_key)"><i class="material-icons">delete</i></button>

                              <script type="text/javascript">
                                //$('button[class*="modal-trigger"]').leanModal();
                              </script>

                              <!-- edit modal -->
                              <div id="modal@{{ sist.system_setting_key }}" class="modal">
                                  <div class="modal-content">
                                      <h4>@{{form_title}}</h4>

                                        {{ csrf_field() }}
                                        <div class="input-field col s12">
                                          <p>@{{sist.system_setting_key}}</p>
                                          <label for="system_setting_key">System Setting Key</label>
                                        </div>

                                      <div class="input-field col s12">
                                        <textarea id="system_setting_value" name="system_setting_value" class="materialize-textarea" ng-model="sistem.system_setting_value"
                                        value="@{{sist.system_setting_value}}"></textarea>
                                        <label for="system_setting_value">System Setting Value</label>
                                      </div>

                                      <div class="input-field col s12">
                                        <input id="updated_by" name="updated_by" type="text" class="validate" length="255"
                                        ng-model="sistem.updated_by" value="@{{sist.updated_by}}">
                                        <label for="updated_by" class="">created_by</label>
                                      </div>
                                    </div>

                                  <div class="modal-footer">
                                    <button type="button" class="modal-action modal-close waves-effect waves-blue btn-flat" id="btn-save" ng-click="edit(modalstate, sist.system_setting_key, sistem.system_setting_value, sistem.updated_by)">Save</button>
                                    <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">cancel</a>
                                  </div>
                              </div>
                              <!-- end edit modal -->
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- ini modal buat create -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4>@{{form_title}}</h4>
            <form>
              {{ csrf_field() }}
            <div class="input-field col s12">
              <input id="system_setting_key" class="validate" name="system_setting_key" type="text" length="255"
              ng-model="sistem.system_setting_key">
              <label for="system_setting_key">System Setting Key</label>
            </div>

            <div class="input-field col s12">
              <textarea id="system_setting_value" name="system_setting_value" class="materialize-textarea" ng-model="sistem.system_setting_value"></textarea>
              <label for="system_setting_value">System Setting Value</label>
            </div>
        </div>
      </form>
        <div class="modal-footer">
            <button type="button" class="modal-action modal-close waves-effect waves-blue btn-flat" id="btn-save" ng-click="save(modalstate, sistem.system_setting_key, sistem.system_setting_value)">Save</button>
            <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">cancel</a>
        </div>
    </div>
</div>

@endsection
