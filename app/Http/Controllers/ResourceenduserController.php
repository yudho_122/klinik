<?php

namespace App\Http\Controllers;

use App\Resource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;
use App\Http\Requests;

class ResourceenduserController extends Controller
{
  public function index() {

      $resources = Resource::orderBy('resource_id', 'asc')->get();

      //return view('enduser.resource.resourcelist');
      return json_encode($resources);
  }
}
