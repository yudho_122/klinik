var app = angular.module('userRecords', ['datatables'])
        .constant('API_URL', 'http://localhost/klinik-hutan/public/api/v1/');

app.controller('userController', function($scope, $timeout ,$http, $window, API_URL) {
    //retrieve employees listing from API
    $window.location;


  //  $scope.groups=[];
    $http.get(API_URL + "user")
        .success(function(response) {
                $scope.user = response;
          });
            //show modal form

   $scope.toggle = function(modalstate, id) {
       $scope.modalstate = modalstate;

       switch (modalstate) {
           case 'add':
               $scope.user = "";
               $scope.form_title = "Add New Admin User";
               break;
           case 'init':
               $('#modal-trigger-' + 'id').leanModal();
           break;
           case 'edit':
               $scope.form_title = "Admin User Detail";
               $scope.email = id;
               $http.get(API_URL + 'user/update/' + id)
                       .success(function(response) {
                           console.log(response);
                           $('label[for*="email"], label[for*="first_name"], label[for*="last_name"], label[for*="password"], label[for*="alamat"], label[for*="telp"], label[for*="gcm_id"], label[for*="user_approval_status"], label[for*="id_number"]').addClass( 'active' );

                            //$scope.user = response;
                            $scope.user.email = response.email;
                            $scope.user.first_name = response.first_name;
                            $scope.user.last_name = response.last_name;
                            $scope.user.password = response.password;
                            $scope.user.alamat = response.alamat;
                            $scope.user.telp = response.telp;
                            $scope.user.gcm_id = response.gcm_id;
                            $scope.user.id_number = response.id_number;

                       });
               break;
           default:
               break;
       }
  }

  var formData = new FormData();

  // save mode_edit
  $scope.edit = function(id, first_name, last_name, password, telp, gcm_id, id_number, alamat) {
      var url = API_URL + "user/edit/" + id;

              formData.append('email', id);
              formData.append('first_name', first_name);
              formData.append('last_name', last_name);
              formData.append('password', password);
              formData.append('telp', telp);
              formData.append('gcm_id', gcm_id);
              formData.append('id_number', id_number);
              formData.append('alamat', alamat);


      //append employee id to the URL if the form is in edit mode
        $http({
          method: 'POST',
          url: url,
          data: formData,
          headers: {'Content-Type':' application/x-www-form-urlencoded'}
      }).success(function(response) {
          console.log(response);
          location.reload();
      }).error(function(response) {
          console.log(response);
          alert('This is embarassing. An error has occured. Please check the log for details');
      });
  }

   //save new record / update existing record
   $scope.save = function(modalstate, id) {
       var url = API_URL + "user";

       //append employee id to the URL if the form is in edit mode
         $http({
           method: 'POST',
           url: url,
           data: $.param($scope.user),
           headers: {'Content-Type': 'application/x-www-form-urlencoded'}
       }).success(function(response) {
           console.log(response);
           location.reload();
       }).error(function(response) {
           console.log(response);
           alert('This is embarassing. An error has occured. Please check the log for details');
       });
   }

   $scope.toEdit = function(id) {
       $window.location.href = '/api/v1/user/edit/' + id;
   }

   $scope.toIndex = function() {
         $window.location.href = '/user';
   }
   //delete record
   $scope.confirmDelete = function(id) {
       var isConfirmDelete = confirm('Are you sure you want this record?');
       if (isConfirmDelete) {
           $http({
               method: 'DELETE',
               url: API_URL + 'user/delete/' + id
           }).
                   success(function(data) {
                       console.log(data);
                       location.reload();
                   }).
                   error(function(data) {
                       console.log(data);
                       alert('Unable to delete');
                   });
       } else {
           return false;
       }
   }

});
