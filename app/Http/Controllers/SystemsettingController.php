<?php

namespace App\Http\Controllers;

use App\Sistem_setting;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\DB;

class SystemsettingController extends Controller
{
  public function index($system_setting_key = null) {

    if ($system_setting_key == null) {
        return Sistem_setting::orderBy('created_at', 'asc')->get();
    } else {
        return $this->show($system_setting_key);
    }
  }

  public function store(Request $request) {
     $validator  = Validator($request->all(), [
      'system_setting_key'   => 'required|max:45',
      'system_setting_value' => 'required|max:45',
      //'updated_by'           => 'required|max:45',
     ]);

     if($validator->fails()) {
            $error = $validator->messages()->toJson();

            echo json_encode(['status' => 'salah', 'message' => $error]);
        } else {
            $system_setting                     = new Sistem_setting;
            $system_setting->system_setting_key     = $request->input('system_setting_key');
            $system_setting->system_setting_value  = $request->input('system_setting_value');
            $system_setting->updated_by            = 'belum di update';

            $result                                  =   $system_setting->save();

            echo json_encode(['status' => 'success', 'message' => $request->all()]);
        }
    }

    public function show($system_setting_key)
    {
        return Sistem_setting::find($system_setting_key);
        //return view('news.show')->withNews($news);
    }

    public function update(Request $request, $system_setting_key) {

     if (!empty($system_setting_key)) {
            $validator  = Validator($request->all(), [
            'system_setting_value' => 'required|max:45',
            'updated_by'           => 'required|max:45',
      ]);

            if($validator->fails()) {
                $error = $validator->messages()->toJson();
                echo json_encode(['status' => 'salah', 'message' => $error]);
            } else {
                $system_setting                         = Sistem_setting::find($system_setting_key);
                $system_setting->system_setting_value  = $request->input('system_setting_value');
                $system_setting->updated_by            = $request->input('updated_by');
                $result                                =   $system_setting->save();

                echo json_encode(['status' => 'success', 'message' => 'data was updated']);
            }
        } else {
            echo json_encode(['status' => 'false', 'message' => 'No parameter Id selected']);
        }
    }

    public function destroy($system_setting_key) {
     $system_setting  = Sistem_setting::findOrFail($system_setting_key);
     $response        = $system_setting->delete();

     echo json_encode(['status' => 'success', 'message' => 'Data was delected']);
    }
}
