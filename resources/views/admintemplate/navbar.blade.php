<aside id="slide-out" class="side-nav white fixed">
                <div class="side-nav-wrapper">
                    <div class="sidebar-profile">
                        <div class="sidebar-profile-image">
                            <img src="{{ asset('admin/assets/images/profile-image.png')}}" class="circle" alt="">
                        </div>
                        <div class="sidebar-profile-info">
                            <a href="javascript:void(0);" class="account-settings-link">
                                <p>David Doe</p>
                                <span>david@gmail.com<i class="material-icons right">arrow_drop_down</i></span>
                            </a>
                        </div>
                    </div>
                    <div class="sidebar-account-settings">
                        <ul>
                            <li class="no-padding">
                                <a class="waves-effect waves-grey"><i class="material-icons">mail_outline</i>Inbox</a>
                            </li>
                            <li class="no-padding">
                                <a class="waves-effect waves-grey"><i class="material-icons">star_border</i>Starred<span class="new badge">18</span></a>
                            </li>
                            <li class="no-padding">
                                <a class="waves-effect waves-grey"><i class="material-icons">done</i>Sent Mail</a>
                            </li>
                            <li class="no-padding">
                                <a class="waves-effect waves-grey"><i class="material-icons">history</i>History<span class="new grey lighten-1 badge">3 new</span></a>
                            </li>
                            <li class="divider"></li>
                            <li class="no-padding">
                                <a href="{{ route('logout') }}" class="waves-effect waves-grey"><i class="material-icons">exit_to_app</i>Sign Out</a>
                            </li>
                        </ul>
                    </div>
                <ul class="sidebar-menu collapsible collapsible-accordion" data-collapsible="accordion">
                    <li class="no-padding"><a class="waves-effect waves-grey active" href="index.html"><i class="material-icons">settings_input_svideo</i>Dashboard</a></li>
                    <li class="no-padding"><a class="waves-effect waves-grey" href="{{ url('/news') }}"><i class="material-icons dp48">receipt</i>News</a></li>
                    <li class="no-padding"><a class="waves-effect waves-grey" href="{{ url('/resource') }}"><i class="material-icons">view_list</i>Resources</a></li>
                    <li class="no-padding"><a class="waves-effect waves-grey" href="{{ url('/clinic') }}"><i class="material-icons">dns</i>Clinic Category</a></li>
                    <li class="no-padding"><a class="waves-effect waves-grey" href="{{ url('/thread') }}"><i class="material-icons">library_books</i>thread blueprint</a></li>
                    <li class="no-padding"><a class="waves-effect waves-grey" href="{{ url('/sis') }}"><i class="material-icons">settings_applications</i>System Setting</a></li>
                    <li class="no-padding"><a class="waves-effect waves-grey" href="{{ url('/group') }}"><i class="material-icons">supervisor_account</i>Group</a></li>
                    <li class="no-padding">
                      <a class="collapsible-header waves-effect waves-grey" href="#"><i class="material-icons">assignment</i>Report<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                        <div class="collapsible-body">
                          <ul>
                              <li><a href="{{ url('/report/thread') }}">Report Thread</a></li>
                              <li><a href="{{ url('/report/resolution') }}">Report Resolution Answer</a></li>
                          </ul>
                        </div>
                  </li>
                    <li class="no-padding">
                      <a class="collapsible-header waves-effect waves-grey" href="#"><i class="material-icons">assignment_ind</i>User<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                        <div class="collapsible-body">
                          <ul>
                              <li><a href="{{ url('/user') }}">Administartor</a></li>
                              <li><a href="#">Tim Expert</a></li>
                              <li><a href="#">Tim Manajemen</a></li>
                              <li><a href="#">End User</a></li>
                          </ul>
                        </div>
                  </li>
                  <li class="no-padding active">
                    <a class="collapsible-header waves-effect waves-grey" href="#"><i class="material-icons">settings_input_svideo</i>Thread<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                      <div class="collapsible-body">
                        <ul>
                            <li><a href="#">Silvikultur</a></li>
                            <li><a href="#">Fisiologi & Genetika</a></li>
                            <li><a href="#">Keteknikan Hutan</a></li>
                            <li><a href="#">Pemodelan</a></li>
                            <li><a href="#">Hasil Hutan</a></li>
                            <li><a href="#">Sosiologi</a></li>
                            <li><a href="#">Kesehatan Hutan</a></li>
                            <li><a href="#">Lingkungan Hutan</a></li>
                            <li><a href="#">Ekonomi dan Kebijakan</a></li>
                            <li><a href="#">Laboratorium Kehutanan</a></li>
                            <li><a href="#">Lainnya</a></li>
                        </ul>
                      </div>
                </li>
                <div class="footer">
                    <p class="copyright">Steelcoders ©</p>
                    <a href="#!">Privacy</a> &amp; <a href="#!">Terms</a>
                </div>
                </div>
            </aside>
