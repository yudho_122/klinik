<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- Title -->
        <title>@yield('title')</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />

        <!-- Styles -->


        <!-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --->
        <link type="text/css" rel="stylesheet" href="{{ asset('admin/assets/plugins/materialize/css/materialize.min.css')}}"/>
        <link href="{{ asset('css/icon.css') }}" rel="stylesheet">
        <link href="{{ asset('admin/assets/plugins/metrojs/MetroJs.min.css')}}" rel="stylesheet">
        <link href="{{ asset('admin/assets/plugins/weather-icons-master/css/weather-icons.min.css')}}" rel="stylesheet">
        <link href="{{ asset('admin/assets/plugins/datatables/css/jquery.dataTables.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.css">
        <link href="{{ asset('admin/assets/plugins/material-preloader/css/materialPreloader.min.css')}}" rel="stylesheet">
        <link href="{{ asset('admin/assets/plugins/google-code-prettify/prettify.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('admin/assets/plugins/nvd3/nv.d3.min.css') }}" rel="stylesheet">
        <link href="{{ asset('admin/assets/plugins/dropzone/dropzone.min.css') }}" rel="stylesheet">
        <link href="{{ asset('admin/assets/plugins/dropzone/basic.min.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/parsley.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/select2.min.css') }}" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <!-- Theme Styles -->
        <link href="{{ asset('admin/assets/css/alpha.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('admin/assets/css/custom.css')}}" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="loader-bg"></div>
        @include('admintemplate.loader')
        <div class="mn-content fixed-sidebar">
            @include('admintemplate.header')

            @include('admintemplate.search')

            @include('admintemplate.chatsidebar')

            @include('admintemplate.chatmessage')

            @include('admintemplate.navbar')

            <main class="mn-inner inner-active-sidebar">

                @yield('content')

                @include('admintemplate.innersidebar')
                <!--@include('admintemplate.modal')-->

            </main>
            @include('admintemplate.footer')
        </div>
        <div class="left-sidebar-hover"></div>

        <!-- Load Javascript Libraries (AngularJS, JQuery, Bootstrap) -->
        <script src="{{ asset('admin/assets/plugins/jquery/jquery-2.2.0.min.js')}}"></script>
        <script src="{{ asset('app/lib/angular/angular.min.js') }}"></script>

        <!--<script src="{{ asset('js/bootstrap.min.js') }}"></script> -->

        <!-- AngularJS Application Scripts -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
        <script src="{{ asset('app/app.js') }}"></script>
        <script src="{{ asset('app/controllers/groups.js') }}"></script>

        @yield('js')
        <!-- Javascripts -->

        <script src="{{ asset('admin/assets/plugins/materialize/js/materialize.min.js')}}"></script>
        <script src="{{ asset('admin/assets/plugins/material-preloader/js/materialPreloader.min.js')}}"></script>
        <script src="{{ asset('admin/assets/plugins/jquery-blockui/jquery.blockui.js')}}"></script>
        <script src="{{ asset('admin/assets/plugins/waypoints/jquery.waypoints.min.js')}}"></script>
        <script src="{{ asset('admin/assets/plugins/counter-up-master/jquery.counterup.min.js')}}"></script>
        <script src="{{ asset('admin/assets/plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
        <script src="{{ asset('admin/assets/plugins/chart.js/chart.min.js')}}"></script>
        <script src="{{ asset('admin/assets/plugins/d3/d3.min.js') }}"></script>
        <script src="{{ asset('admin/assets/plugins/nvd3/nv.d3.min.js') }}"></script>
        <!-- <script src="{{ asset('admin/assets/plugins/flot/jquery.flot.js') }}"></script>
        <script src="{{ asset('admin/assets/plugins/flot/jquery.flot.min.js') }}"></script>
        <script src="{{ asset('admin/assets/plugins/flot/jquery.flot.time.min.js')}}"></script>
        <script src="{{ asset('admin/assets/plugins/flot/jquery.flot.symbol.min.js')}}"></script>
        <script src="{{ asset('admin/assets/plugins/flot/jquery.flot.resize.min.js')}}"></script>
        <script src="{{ asset('admin/assets/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
        <script src="{{ asset('admin/assets/plugins/flot/jquery.flot.pie.min.js') }}"></script> -->
        <script src="{{ asset('admin/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') }}"></script>
        <script src="{{ asset('admin/assets/plugins/peity/jquery.peity.min.js')}}"></script>
        <script src="{{ asset('admin/assets/js/alpha.min.js')}}"></script>
        <script src="{{ asset('admin/assets/js/pages/dashboard.js')}}"></script>
        <script src="{{ asset('admin/assets/js/pages/form_elements.js')}}"></script>
        <script src="{{ asset('admin/assets/js/pages/table-data.js')}}"></script>
        <script src="{{ asset('admin/assets/js/pages/modalMd.js')}}"></script>
        <script src="{{ asset('/js/angular-datatables.min.js') }}"></script>

        <script src="{{ asset('admin/assets/plugins/datatables/js/jquery.dataTables.min.js')}}"></script>
        <script src="{{ asset('admin/assets/plugins/google-code-prettify/prettify.js')}}"></script>
        <script src="{{ asset('admin/assets/js/pages/ui-modals.js')}}"></script>
        <script src="{{ asset('admin/assets/plugins/dropzone/dropzone.min.js') }}"></script>
        <script src="{{ asset('admin/assets/plugins/dropzone/dropzone-amd-module.min.js') }}"></script>
        <script src="{{ asset('/js/parsley.min.js') }}"></script>
        <script src="{{ asset('/js/select2.min.js') }}"></script>
        <script src="{{ asset('app/controllers/newss.js') }}"></script>
        <script src="{{ asset('app/controllers/resource.js') }}"></script>
        <script src="{{ asset('app/controllers/clinic_category.js') }}"></script>
        <script src="{{ asset('app/controllers/thread.js') }}"></script>
        <script src="{{ asset('app/controllers/Comment.js') }}"></script>
        <script src="{{ asset('app/controllers/sistem_setting.js') }}"></script>
        <script src="{{ asset('app/controllers/user.js') }}"></script>

    </body>
</html>
