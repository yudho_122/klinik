@extends('admintemplate.layout')

@section('content')
<div class="row">
    <div class="col s12">
        <div class="page-title">Master Data</div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <span class="card-title">Laboratorium Kehutanan</span>
                <!-- <p>This is the news list that has been written all the time</p><br> -->
                <button class="waves-effect waves-light btn m-b-xs modal-trigger" href="#modal1"><i class="material-icons left">add_box</i>Tambah</button>
                <div id="modal1" class="modal">
                    <div class="modal-content">
                      <form>
                        <h4>Create New Thread</h4>
                              <div class="input-field">
                                  <input id="first_name" type="text" class="validate">
                                  <label for="first_name">Subject</label>
                              </div>
                              <div class="input-field">
                                  <input id="first_name" type="text" class="validate">
                                  <label for="first_name">Detail</label>
                              </div>
                              <div class="input-field">
                                  <input id="first_name" type="text" class="validate">
                                  <label for="first_name">Thread Status</label>
                              </div>
                              <div class="input-field">
                                  <input id="first_name" type="text" class="validate">
                                  <label for="first_name">Create By</label>
                              </div>
                              <div class="input-field">
                                  <input id="first_name" type="text" class="validate">
                                  <label for="first_name">Update By</label>
                              </div>
                              <div class="input-field">
                                  <input id="first_name" type="text" class="validate">
                                  <label for="first_name">Resoluttion Answer</label>
                              </div>
                    </form>
                    </div>
                    <div class="modal-footer">
                      <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Save</a>
                      <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
                    </div>
              </div>
                <table id="example" class="display responsive-table datatable-example">
                    <thead>
                        <tr>
                            <th>Subject</th>
                            <th>Create At</th>
                            <th>Create By</th>
                            <th>Thread Status</th>
                            <th align="center">Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                          <th>Subject</th>
                          <th>Create At</th>
                          <th>Create By</th>
                          <th>Thread Status</th>
                          <th align="center">Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <tr>
                            <td>disini judul</td>
                            <td>2 jan</td>
                            <td>Oi</td>
                            <td>Waiting</td>
                            <td><a href="#" class="waves-effect waves-light btn m-b-xs">View</a></td>
                      </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
