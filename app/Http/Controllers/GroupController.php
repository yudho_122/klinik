<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use App\Group;
use Session;
use Purifier;


class GroupController extends Controller
{
    public function index($group_id = null) {

      if ($group_id == null) {
          return Group::orderBy('group_id', 'asc')->get();
      } else {
          return $this->show($group_id);
      }
    }


    public function create(Request $request) {
    	$validator 	=	Validator($request->all(), [
    		'group_id' => 'required|max:45',
    		'group_name' => 'required|max:45',
    		'group_des' => 'required',
    		'created_by' => 'required|max:45',
    	]);

    	if($validator->fails()) {
            $error = $validator->messages()->toJson();

            echo json_encode(['status' => 'salah', 'message' => $error]);
        } else {
            $group 				=	new Group;
            $group->group_id 	=	$request->input('group_id');
            $group->group_name 	=	$request->input('group_name');
            $group->group_des 	=	$request->input('group_des');
            $group->created_by 	=	$request->input('created_by');
            $result                     =   $group->save();

            echo json_encode(['status' => 'success', 'message' => $request->all()]);
        }
    }

    public function show($group_id)
    {
        return Group::find($group_id);
        //return view('news.show')->withNews($news);
    }

    public function getGroup(Request $request) {
    	$group_id 	=	$request->input('group_id');
    	$group       =   Group::find($group_id);

        echo json_encode($group);
    }

    public function update(Request $request, $group_id) {
    	$group_id 	=	$request->input('group_id');
    	if (!empty($group_id)) {
            $validator 	=	Validator($request->all(), [
	    		'group_id' => 'required|max:45',
	    		'group_name' => 'required|max:45',
	    		'group_des' => 'required',
	    		'created_by' => 'required|max:45',
	    	]);

            if($validator->fails()) {
                $error = $validator->messages()->toJson();
                echo json_encode(['status' => 'salah', 'message' => $error]);
            } else {
                $group       =   Group::find($group_id);
                $group->group_id 	=	$request->input('group_id');
	            $group->group_name 	=	$request->input('group_name');
	            $group->group_des 	=	$request->input('group_des');
	            $group->created_by 	=	$request->input('created_by');
	            $result             =   $group->save();

                echo json_encode(['status' => 'success', 'message' => 'data was updated']);
            }
        } else {
            echo json_encode(['status' => 'false', 'message' => 'No parameter Id selected']);
        }
    }

    public function destroy($id) {
    	$group 	=	Group::findOrFail($id);
    	$response 	=	$group->delete();

    	echo json_encode(['status' => 'success', 'message' => 'Data was delected']);
    }
}
