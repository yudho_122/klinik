<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{

  protected $table = 'permissions';
  public $incrementing = false;
  public $timestamps = false;

//  protected $fillable = ['group_id','permission_key','permission_value'];

  public group(){
    return $this->belongsTo('App\Group', 'group_id', 'group_id');
  }
}
