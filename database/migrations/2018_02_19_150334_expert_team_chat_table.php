<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExpertTeamChatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('expert_team_chat', function (Blueprint $table) {
          $table->bigIncrements('expert_team_chat_id')->unsigned();
          $table->string('expert_team_chat_from', 45)->nullable();
          $table->string('expert_team_chat_to', 45)->nullable();
          $table->text('content')->nullable();
          $table->string('type', 45)->nullable();
          $table->dateTime('created_at');
          $table->text('message_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('expert_team_chat');
    }
}
