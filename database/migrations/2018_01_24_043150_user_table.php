<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
   public function up(){
     Schema::create('users', function (Blueprint $table) {
       $table->string('email', 45)->unique();
       $table->primary('email');
       $table->string('first_name', 45);
       $table->string('last_name', 45)->nullable();
       $table->text('password');
       $table->timestamps();
       $table->string('group_id', 45);
       $table->foreign('group_id')->references('group_id')->on('groups');
       $table->text('alamat')->nullable();
       $table->string('telp', 20)->nullable();
       $table->text('gcm_id')->nullable();
       $table->enum('user_approval_status', array('WAITING', 'APPROVE','REJECTED'))->default('WAITING');
       $table->string('id_number', 45);

     });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down(){
        Schema::drop('users');
     }
}
