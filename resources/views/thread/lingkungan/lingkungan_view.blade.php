@extends('admintemplate.layout')

@section('content')

	<div class="row">
		<div class="col s8">
			<h1>NEWS TITLE DI SINI YA</h1>
			<p class="lead">Lingkungan Hutan</p>

			<h3>Comments <small>total</small></h3>

				<table class="table">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Comment</th>
							<th width="70px"></th>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td>nama</td>
							<td>Comment</td>
						</tr>
					</tbody>
				</table>

		</div>

      <div class="col s4">
        <div class="card white">
          <dl class="card-content">
            <label>Url:</label>
              <p><a href="#">ini url Thread</a></p>
          </dl>
          <dl class="card-content">
            <label>Url:</label>
            <p><a href="#">thread status</a></p>
          </dl>
          <dl class="card-content">
            <label>Created At:</label>
            <p>tanggal created at</p>
          </dl>

          <dl class="card-content">
            <label>Last Updated:</label>
            <p>Tanggal updated at</p>
          </dl>
          <dl class="card-content">
            <label>Created By:</label>
            <p>Created By siapa</p>
          </dl>
          <dl class="card-content">
            <label>Update By:</label>
            <p>Update By siapa</p>
          </dl>
          <dl class="card-content">
            <label>Resolution Answer</label>
            <p>Jawaban akhir</p>
          </dl>

        <hr>
          <div class="row">
            <div class="col s6">
              <button class="waves-effect waves-light btn m-b-xs modal-trigger" href="#modal1" style="width: 150px;">Edit</button>
                <div id="modal1" class="modal">
                  <div class="modal-content">
                  <form>
                    <h4>Create New Thread</h4>
                          <div class="input-field">
                              <input id="first_name" type="text" class="validate">
                              <label for="first_name">Subject</label>
                          </div>
                          <div class="input-field">
                              <input id="first_name" type="text" class="validate">
                              <label for="first_name">Detail</label>
                          </div>
                          <div class="input-field">
                              <input id="first_name" type="text" class="validate">
                              <label for="first_name">Thread Status</label>
                          </div>
                          <div class="input-field">
                              <input id="first_name" type="text" class="validate">
                              <label for="first_name">Create By</label>
                          </div>
                          <div class="input-field">
                              <input id="first_name" type="text" class="validate">
                              <label for="first_name">Update By</label>
                          </div>
                          <div class="input-field">
                              <input id="first_name" type="text" class="validate">
                              <label for="first_name">Resoluttion Answer</label>
                          </div>
                    </form>
                 </div>
                <div class="modal-footer">
                  <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Save</a>
                  <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
                </div>
            </div>
          </div>

          <div class="col s6">
            <!--<a href="#" class="waves-effect waves-light btn red m-b-xs" style="width: 150px;">Delete</a>-->
            <button class="waves-effect waves-light btn m-b-xs modal-trigger red" href="#modal3" style="width: 150px;">Delete</button>
                <div class="modal" id="modal3">
                    <div class="modal-content">
                      <h4>Delete Group</h4>
                              <a>Apakah anda yakin untuk menghapus?</a>
                              <div class="input-field">
                                <!--  <input id="first_name" type="text" class="validate">-->
                                  <button class="waves-effect waves-light btn m-b-xs modal-trigger">Yes</button>
                                  <button class="waves-effect waves-light btn m-b-xs modal-trigger">No</button>
                              </div>
                    </div>
              </div>
          </div>
          <div class="col s12 m6 l8">
            <a href="#" class="waves-effect waves-light btn blue m-b-xs" style="width: 323px;">See All News</a>
          </div>
        </div>
        <div class="row">

        </div>
      </div>
      </div>
    </div>

@endsection
