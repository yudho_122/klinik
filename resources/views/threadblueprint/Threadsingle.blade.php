@extends('threadblueprint.layoutsingle')

@section('content')

	<div class="col-lg-8 slideanim">

		<div class="hover01 column">
			<div>
				<figure><img src="#" class="img-responsive" width="auto" height="auto" /></figure>
			</div>
		</div>

			<h3 class="ctitle">#</h3>
			<p class="blog-p1"><small>Published: #</small></p>

			<p class="blog-p2">#</p>
			<hr>
			<p>Posted In: #</p>
			<div class="spacing"></div>

			<h4>SHARE:</h4>
			<ul class="list-inline">
									<li>
											<a href="#" class="btn-social"><i class="fa fa-fw fa-facebook"></i></a>
									</li>
									<li>
											<a href="#" class="btn-social"><i class="fa fa-fw fa-google-plus"></i></a>
									</li>
									<li>
											<a href="#" class="btn-social"><i class="fa fa-fw fa-twitter"></i></a>
									</li>
									<li>
											<a href="#" class="btn-social"><i class="fa fa-fw fa-linkedin"></i></a>
									</li>
									<li>
											<a href="#" class="btn-social"><i class="fa fa-fw fa-dribbble"></i></a>
									</li>
							</ul>


	<!-- comment -->

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h3 class="comments-title"><span class="glyphicon glyphicon-comment"></span>  #</h3>

				<div class="comment">
					<div class="author-info">

						<img src="" class="author-image">
						<div class="author-name">
							<h4>#</h4>
							<p class="author-time">#</p>
						</div>

					</div>

					<div class="comment-content">

					</div>

				</div>

		</div>
	</div>

	<div class="row">
		<div id="comment-form" class="comment-w3ls" style="margin-top: 50px;">

			<div class="well">


				<div class="row">
					<div class="col-md-6">

					</div>

					<div class="col-md-6">

					</div>

					<div class="col-md-12">

					</div>
				</div>


		</div>

		</div>
	</div>
</div>

@endsection
