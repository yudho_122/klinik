var app = angular.module('commentRecords', ['datatables'])
        .constant('API_URL', 'http://localhost/klinik-hutan/public/api/v1/');

app.controller('commentController', function($scope, $timeout ,$http, $window, API_URL) {
    //retrieve employees listing from API
    $window.location;

    $http.get(API_URL + "comment")
            .success(function(response) {
                $scope.comment = response;
            });

    var formData = new FormData();

    //save new record / update existing record
    $scope.save = function(id, comment, created_by) {
        var url = API_URL + "comment/" + id;

        formData.append('comment', comment);
        formData.append('thread_case_id', id);
        formData.append('created_by', created_by);

        $http({
            method: 'POST',
            url: url,
            data: formData,
            headers: {'Content-Type': undefined},
        }).success(function(response) {
            console.log(response);
            location.reload();
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }


});
