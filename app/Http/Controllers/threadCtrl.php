<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Thread_case;
use App\Clinic_category;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;


class threadCtrl extends Controller
{
  public function index($thread_case_id = null) {

        //$clinic_categorys = Clinic_category::all();

        if ($thread_case_id == null) {
          $thread   = DB::table('thread_cases')
                        ->join('clinic_categorys', 'thread_cases.clinic_category_id', '=', 'clinic_categorys.clinic_category_id')
                        ->get();
            return $thread;

            //return view('thread.index')->withThread($thread);
        } else {
            return $this->show($thread_case_id);
        }

  }

  public function tampil($thread_case_id) {
    $thread = Thread_case::find($thread_case_id);
    return view('thread.view')->with('thread', $thread);
  }

  public function show($thread_case_id){
      return Thread_case::find($thread_case_id);
      return view('thread.view')->with('thread', $thread);
  }

  public function create() {
        $clinic_categorys = Clinic_category::all();

        return view('thread.create')->withClinic_categorys($clinic_categorys);
  }

  public function store(Request $request)
  {

    $validator 	=	Validator::make($request->all(), [
      'subject'              =>   'required',
      'created_by' 		       => 	'required|max:45',
      'clinic_category_id'   => 'required|integer',
      'detail' 				       =>	  'required',
    ]);

    if($validator->fails()) {
          $error = $validator->messages()->toJson();

          echo json_encode(['status' => 'salah', 'message' => $error]);
      } else {
          $thread 						        =	new Thread_case;
          $thread->thread_status 			=	'OPEN';
          $thread->clinic_category_id	=	$request->input('clinic_category_id');
          $thread->subject            = $request->input('subject');
          $thread->created_by 			  =	$request->input('created_by');
          $thread->detail 				    =	$request->input('detail');
          $result 						        =	$thread->save();

          echo json_encode(['status' => 'success', 'message' => $request->all()]);
      }
  }

  public function destroy($thread_case_id) {
    $thread 	=	Thread_case::findOrFail($thread_case_id);

    $response 	=	$thread->delete();

    echo json_encode(['status' => 'success', 'message' => 'Data was delected']);
  }
}
