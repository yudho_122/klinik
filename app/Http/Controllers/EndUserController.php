<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

class EndUserController extends Controller
{

    public function index() {
        $news   =   News::all();

        return response()->json($news);

    }

    public function create(Request $request) {
        $validator = Validator::make($request->all(), [
            'news_title'        => 'required|max:255',
            'news_picture_path' => 'required|mimes:jpeg,png|max:2048',
            'created_by'        => 'required|max:45',
        ]);

        if($validator->fails()) {
            $error = $validator->messages()->toJson();

            echo json_encode(['status' => 'salah', 'message' => $error]);
        } else {
            $file   =   time().'.'.$request->news_picture_path->getClientOriginalExtension();
            $request->news_picture_path->move(public_path('images/news'), $file);
            $path   =   public_path('images/news/'). $file;

            $news                       =   new News;
            $news->news_title           =   $request->input('news_title');
            $news->news_content         =   $request->input('news_content');
            $news->news_picture_path    =   $path;
            $news->created_by           =   $request->input('created_by');
            $result                     =   $news->save();

            echo json_encode(['status' => 'success', 'message' => $request->all()]);
        }
    }

    public function getEnduser(Request $request) {
        $news_id    =   $request->news_id;
        $news       =   News::find($news_id);

        echo json_encode($news);
    }

    public function update(Request $request) {
        $news_id    =   $request->input('news_id');
        if (!empty($news_id)) {
            $validator = Validator::make($request->all(), [
                'news_title' => 'required|max:255',
                'news_picture_path' => 'required',
                'created_by' => 'required|max:45',
            ]);
            if($validator->fails()) {
                $error = $validator->messages()->toJson();

                echo json_encode(['status' => 'salah', 'message' => $error]);
            } else {
                $news       =   News::find($news_id);
                $news->news_title     =   $request->input('news_title');
                $news->news_content     =   $request->input('news_content');
                $ex_name    =   $news->news_picture_path;
                $new_name   =   substr($ex_name, -14);
                if($request->hasFile('news_picture_path')) {
                    if (File::exists(base_path()) ."/public/images/news/". $new_name) {
                      File::delete(base_path() ."/public/images/news/". $new_name);
                    }
                    $file   =   time().'.'.$request->news_picture_path->getClientOriginalExtension();
                    $request->news_picture_path->move(public_path('images/news'), $file);
                    $path   =   public_path('images/news/'). $file;
                    $news->news_picture_path = $path;
                }
                $news->created_by     =   $request->input('created_by');
                $result     =   $news->save();

                echo json_encode(['status' => 'success', 'message' => 'data was updated']);
            }
        } else {
            echo json_encode(['status' => 'false', 'message' => 'No parameter Id selected']);
        }
    }

    public function destroy($id) {
    	$news   =   News::findOrFail($id);
        $filename   =   $news->news_picture_path;
        $rest = substr($filename, -14);
        if (File::exists(base_path()) ."/public/images/news/". $rest) {
          File::delete(base_path() ."/public/images/news/". $rest);
        }
        $response   =   $news->delete();

        echo json_encode(['status' => 'success', 'message' => 'Data was deleted']);
    }

}
