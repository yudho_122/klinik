<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Laporan Thread</title>
    <style type="text/css">
	   img {
        display: block;
        margin: 0 auto;
      }
    </style>
  </head>

  <body>
    <table width=100%>
      <tr>
        <td align="right" width=20%><img src="{{ asset('enduser\images\logo.png')}}" height="75" width="75"></td>
        <td align="center" width=70%><h3 style="text-align: center;"><strong>KEMENTRIAN LINGKUNGAN HIDUP DAN KEHUTANAN
        <br>BADAN PENELITIAN, PENGEMBANGAN DAN INOVASI</strong></h3></td>
        <td align="right" width=5%></td>
      </tr>
    </table>
    <hr size"1" color="black">
    <hr size"5" color="black">
    <p>&nbsp;</p>
    <p style="text-align: center;"><strong>LAPORAN JUMLAH KASUS</strong></p>
    <p style="text-align: center;"><strong>PERIODE : {{$start}} sampai {{$end}}</strong></p>
    <p style="text-align: center;">&nbsp;</p>

    <table style=" border-collapse: collapse; border-color: black; margin-left: auto; margin-right: auto;" border="2" width="600">
      <thead>
          <tr>
              <th>Subject</th>
              <th>Category</th>
              <th>Created At</th>
              <th>Created By</th>
              <th>Jumlah Comment</th>
              <th>Status Thread</th>
          </tr>
      </thead>
      <tbody>
        @foreach ($report as $repor)
        <tr style="height: 28px">
          <td align="center" style="width: 250px; height: 28px;">{{$repor->subject}}</td>
          <td align="center" style="width: 109px; height: 28px;">{{$repor->clinic_category_name}}</td>
          <td align="center" style="width: 109px; height: 28px;">{{$repor->created_at}}</td>
          <td align="center" style="width: 109px; height: 28px;">{{$repor->created_by}}</td>
          <td align="center" style="width: 109px; height: 28px;">{{$repor->comment}}</td>
          <td align="center" style="width: 109px; height: 28px;">{{$repor->thread_status}}</td>
        </tr>
        @endforeach
      </tbody>
    <table>
  </body>

</html>
