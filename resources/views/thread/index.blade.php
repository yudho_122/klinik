@extends('admintemplate.layout')

@section('content')
<br>

<div class="row" ng-app="threadRecords" ng-controller="threadsController">
    <div class="col s12">
        <div class="page-title">Master Data</div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <span class="card-title">THREAD</span>
                <!-- <p>This is the news list that has been written all the time</p><br> -->
                <button class="waves-effect waves-light btn blue m-b-xs modal-trigger" href="#" ng-click="toCreate()"><i class="material-icons left">add_box</i>Tambah</button>
                <table id="example" class="display responsive-table datatable-example">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Thread Subject</th>
                            <th>Category Name</th>
                            <th>Thread Detail</th>
                            <th>Thread Status</th>
                            <th>Created at</th>
                            <th>Created by</th>
                            <th>Operation</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                          <th>ID</th>
                          <th>Thread Subject</th>
                          <th>Category Name</th>
                          <th>Thread Detail</th>
                          <th>Thread Status</th>
                          <th>Created at</th>
                          <th>Created by</th>
                          <th>Operation</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <tr ng-repeat="the in thread" id="text-tr">
                            <td>@{{ the.thread_case_id }}</td>
                            <td>@{{ the.subject }}</td>
                            <td>@{{ the.clinic_category_name }}</td>
                            <td>@{{ the.detail }}</td>
                            <td>@{{ the.thread_status }}</td>
                            <td>@{{ the.created_at }}</td>
                            <td>@{{ the.created_by }}</td>
                            <td style="width: 30%;">
                              <a class="btn indigo" style="bottom: 10px; right: 100px;" href="{{ url('/api/v1/thread/view') }}/@{{ the.thread_case_id }}"><i class="material-icons">info</i></a>
                              <button id="modal-trigger-@{{ the.thread_case_id }}" class="waves-effect waves-light btn blue m-b-xs modal-trigger" style="bottom: 10px; left: 0.5px; top: 5px;" href="#modal@{{ the.thread_case_id }}" ng-click="toggle('edit', the.thread_case_id)" ng-mouseover="toggle('init', the.thread_case_id)"><i class="material-icons">mode_edit</i></button>
                              <button class="btn red" style="bottom: 10px; left: 200px;" ng-click="confirmDelete(the.thread_case_id)"><i class="material-icons">delete</i></button>

                              <script type="text/javascript">
                                //$('button[class*="modal-trigger"]').leanModal();
                              </script>

                              <!-- edit modal -->
                              <div id="modal@{{ the.thread_case_id }}" class="modal">
                                  <div class="modal-content">
                                      <h4>@{{form_title}}</h4>

                                        {{ csrf_field() }}
                                      <div class="input-field col s12">
                                        <input id="subject" class="validate subject" name="subject" type="text" length="255"
                                        ng-model="thread.subject" value="@{{the.subject}}">
                                        <label for="subject">Thread Subject</label>
                                      </div>

                                      <div class="input-field col s12">
                                        <input id="clinic_category_id" class="validate clinic_category_id" name="subject" type="text" length="255"
                                        ng-model="thread.clinic_category_id" value="@{{the.clinic_category_id}}">
                                        <label for="clinic_category_id">clinic_category_id</label>
                                      </div>

                                      <div class="input-field col s12">
                                        <textarea id="detail" name="detail" class="materialize-textarea" ng-model="thread.detail"
                                        value="@{{the.detail}}"></textarea>
                                        <label for="detail">Thread Detail</label>
                                      </div>

                                      <div class="input-field col s12">
                                        <input id="created_by" name="created_by" type="text" class="validate" length="255"
                                        ng-model="thread.created_by" value="@{{the.created_by}}">
                                        <label for="created_by" class="">created_by</label>
                                      </div>
                                      <button type="button" class="waves-effect waves-light btn blue m-b-xs" id="btn-save" ng-click="edit(modalstate, the.thread_case_id, thread.subject, thread.detail, thread.created_by)">Save</button>
                                  </div>

                                  <div class="modal-footer">
                                      <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">cancel</a>
                                  </div>
                              </div>
                              <!-- end edit modal -->
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- ini modal buat create -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4>@{{form_title}}</h4>
            <form>
              {{ csrf_field() }}
            <div class="input-field col s12">
              <input id="subject" class="validate" name="subject" type="text" length="255"
              ng-model="thread.subject">
              <label for="subject">subject</label>
            </div>

            <div class="input-field col s12">

                  <label>Select Clinic Category</label>
            </div>

            <div class="input-field col s12">
              <textarea id="detail" name="detail" class="materialize-textarea" ng-model="thread.detail"></textarea>
              <label for="detail">Thread Detail</label>
            </div>

            <div class="input-field col s12">
              <input id="created_by" name="created_by" type="text" class="validate" length="255"
              ng-model="thread.created_by">
              <label class="">created by</label>
            </div>
            <button type="button" class="waves-effect waves-light btn blue m-b-xs" id="btn-save" ng-click="save(modalstate, thread.thread_case_id, thread.subject, thread.clinic_category_id, thread.detail, thread.created_by)">Save</button>
        </div>
      </form>
        <div class="modal-footer">
            <button type="button">close</button>
        </div>
    </div>
</div>

@endsection
