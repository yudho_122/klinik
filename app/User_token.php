<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_token extends Model
{
      protected $table = 'user_tokens';
      public $incrementing = false;

     protected $fillable = ['user_email','user_token','created_at','expired_at'];

      public $timestamps = false;

      public function user(){
        return $this->belongsTo('App\User', 'user_email', 'email');
      }

}
