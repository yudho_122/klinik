var app = angular.module('threadRecords', ['datatables'])
        .constant('API_URL', 'http://localhost/klinik-hutan/public/api/v1/');

app.controller('threadsController', function($scope, $timeout ,$http, $window, API_URL) {
    //retrieve employees listing from API
    $scope.files = [];
    $scope.errors = [];
    $window.location;

    $http.get(API_URL + "thread")
            .success(function(response) {
                $scope.thread = response;
            });

    //show modal form
    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.thread = "";
                $scope.form_title = "Add New Thread";
                break;
            case 'init':
              $('#modal-trigger-' + id).leanModal();
            break;
            case 'edit':
              $scope.form_title = "Thread Detail";
              $scope.thread_case_id = id;

              $http.get(API_URL + 'thread/' + id)
                      .success(function(response) {
                          console.log(response);

                          //-- set form to focus
                          $('label[for*="subject"], label[for*="detail"], label[for*="created_by"]').addClass( 'active' );

                          $scope.thread.subject     = response.subject;
                          $scope.thread.detail      = response.detail;
                          $scope.thread.created_by  = response.created_by;

                      });

                break;
            default:
                break;
        }

    }

    //-- instansiasi object formData
    var formData = new FormData();

    $scope.setTheFiles = function ($files) {
        angular.forEach($files, function (value, key) {
            formData.append('resource_path', value);
        });
    };


    $scope.edit = function(modalstate, id, subject, clinic_category_id, detail, created_by){
        var url = API_URL + "thread/" + id;

        formData.append('subject', subject);
        formData.append('clinic_category_id', clinic_category_id);
        formData.append('detail', detail);
        formData.append('created_by', created_by);

        $http({
            method: 'POST',
            url: url,
            data: formData,
            headers: {'Content-Type': undefined},
        }).success(function(response) {
            console.log(response);
            location.reload();
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    };

    //save new record / update existing record
    $scope.save = function(modalstate, id, subject, clinic_category_id, detail, created_by) {
        var url = API_URL + "thread";

        //append employee id to the URL if the form is in edit mode
        if (modalstate === 'edit' && modalstate === 'init'){
            url += "/" + id;
        }

        formData.append('subject', subject);
        formData.append('clinic_category_id', clinic_category_id);
        formData.append('detail', detail);
        formData.append('created_by', created_by);

        $http({
            method: 'POST',
            url: url,
            data: formData,
            headers: {'Content-Type': undefined},
        }).success(function(response) {
            console.log(response);
            location.reload();
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

    //redirect ke /news alias news_index
    $scope.toCreate = function() {
          $window.location.href = '/api/v1/thread/create';
    }

    $scope.toIndex = function() {
          $window.location.href = '/thread';
    }

    //delete record
    $scope.confirmDelete = function(id) {
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'thread/' + id
            }).
                    success(function(data) {
                        console.log(data);
                        //location.reload();
                        $window.location.href = '/thread';
                        //$window.location.href;

                    }).
                    error(function(data) {
                        console.log(data);
                        alert('Unable to delete');
                    });
        } else {
            return false;
        }

    }
});

app.directive('ngFiles', ['$parse', function ($parse) {

    function file_links(scope, element, attrs) {
        var onChange = $parse(attrs.ngFiles);
        element.on('change', function (event) {
            onChange(scope, {$files: event.target.files});
        });
    }

    return {
        link: file_links
    }
}]);
