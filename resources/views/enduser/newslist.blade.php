<!DOCTYPE html>
<html lang="en">
<head>
  <title>Klinik Hasil Hutan</title>
  <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="{{ asset('enduser/newslist/images/logo.png')}}" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="{{ asset('enduser/newslist/css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('enduser/newslist/css/font-awesome.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('enduser/newslist/css/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('enduser/newslist/css/slick.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('enduser/newslist/css/theme.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('enduser/newslist/css/style.css')}}">

  <style>

    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }

    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;

    }
  </style>
</head>
<body style="background-image: url('enduser/newslist/photo/bg-1.jpg');" >
<div id="preloader">
  <div id="status"></div>
</div>
<div class="container">
      <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="home.html">
      	  <img src="{{ asset('enduser/newslist/images/logo.png')}}" style="width:45px; margin-left: 5px" alt="Image">
      	  </a>
          </div>
          <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
              <li class="active"><a href="home.html">Home</a></li>

              <li><a href="#">KPI</a></li>
              <li><a href="resource/resource.html">Resource</a></li>
      		<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Task<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Page 1-1</a></li>
                <li><a href="#">Page 1-2</a></li>
                <li><a href="#">Page 1-3</a></li>
              </ul>
            </li>
            </ul>
      	    <form class="navbar-form navbar-left" action="/action_page.php">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search" name="search">
                 <div class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                  <i class="glyphicon glyphicon-search"></i>
                  </button>
                </div>
              </div>
            </form>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Notification (<b>2</b>)
             <span class="glyphicon glyphicon-bell"></span></a>
              <ul class="dropdown-menu notify-drop">
                <div class="notify-drop-title">
                    <div class="row">
                      <div class="col-md-6 col-sm-6 col-xs-6">Request Pesan (<b>2</b>)</div>
                      <div class="col-md-6 col-sm-6 col-xs-6 text-right"><a href="" class="rIcon allRead" data-tooltip="tooltip" data-placement="bottom" title="tümü okundu."><i class="fa fa-dot-circle-o"></i></a></div>
                    </div>
                </div>
            <div class="drop-content">
              <li>
                <div class="col-md-3 col-sm-3 col-xs-3"><div class="notify-img"><img src="http://placehold.it/45x45" alt=""></div></div>
                <div class="col-md-9 col-sm-9 col-xs-9 pd-l0"><a href="">Comel</a> menyukai sebuah kiriman di <a href="">Dota 2</a> <a href="" class="rIcon"><i class="fa fa-dot-circle-o"></i></a>
                <p class="time">Baru saja</p>
                </div>
              </li>
              <li>
                <div class="col-md-3 col-sm-3 col-xs-3"><div class="notify-img"><img src="http://placehold.it/45x45" alt=""></div></div>
                <div class="col-md-9 col-sm-9 col-xs-9 pd-l0"><a href="">Yudhono</a> menyukai kiriman <a href="">Abdullah</a> <a href="" class="rIcon"><i class="fa fa-dot-circle-o"></i></a>
                <p>Lorem ipsum sit dolor amet consilium.</p>
                <p class="time">1 menit yang lalu</p>
                </div>
              </li>
              <li>
                <div class="col-md-3 col-sm-3 col-xs-3"><div class="notify-img"><img src="http://placehold.it/45x45" alt=""></div></div>
                <div class="col-md-9 col-sm-9 col-xs-9 pd-l0"><a href="">Abdullah</a> mengirim sesuatu ke <a href="">Gan Numpang nanya dong</a> <a href="" class="rIcon"><i class="fa fa-dot-circle-o"></i></a>
                <p>Lorem ipsum sit dolor amet consilium.</p>
                <p class="time">29 menit yang lalu</p>
                </div>
              </li>
              <li>
                <div class="col-md-3 col-sm-3 col-xs-3"><div class="notify-img"><img src="http://placehold.it/45x45" alt=""></div></div>
                <div class="col-md-9 col-sm-9 col-xs-9 pd-l0"><a href="">Ahmet</a> menjawab <a href="">icikiwir</a> <a href="" class="rIcon"><i class="fa fa-dot-circle-o"></i></a>
                <p>HEYO WAZAP MAMEN</p>
                <p class="time">hari ini 13:18</p>
                </div>
              </li>
              <li>
                <div class="col-md-3 col-sm-3 col-xs-3"><div class="notify-img"><img src="http://placehold.it/45x45" alt=""></div></div>
                <div class="col-md-9 col-sm-9 col-xs-9 pd-l0"><a href="">kawkaw</a> menjawab <a href=""> tang ina</a> <a href="" class="rIcon"><i class="fa fa-dot-circle-o"></i></a>
                <p>Lorem ipsum sit dolor amet consilium.</p>
                <p class="time">2 jam yang lalu</p>
                </div>
              </li>
            </div>
            <div class="notify-drop-footer text-center">
              <a href=""><i class="fa fa-eye"></i> View All</a>
            </div>
            </ul>
              <li><a href="login.html"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>&nbsp;&nbsp;&nbsp;&nbsp;
            </ul>
          </div>
        </div>
      </nav>
  <div class="content_middle">
    <div class="jumbotron">
      <br></br>
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
          <div class="item active">
            <img src="{{ asset('enduser/newslist/photo/fr1.jpg')}}" alt="Los Angeles" style="width:100%;">
            <div class="carousel-caption">
              <h5>Jokowi Ajak Rimbawan UGM Atasi Belum Optimalnya Pengelolaan Hutan</h5>

            </div>
          </div>

          <div class="item">
            <img src="{{ asset('enduser/newslist/photo/fr3.jpg')}}" alt="Chicago" style="width:100%;">
            <div class="carousel-caption">
              <h5>Jokowi Ajak Rimbawan UGM Atasi Belum Optimalnya Pengelolaan Hutan</h5>

            </div>
          </div>

          <div class="item">
            <img src="{{ asset('enduser/newslist/photo/fr3.jpg')}}" alt="New york" style="width:100%;">
            <div class="carousel-caption">
              <h5>Jokowi Ajak Rimbawan UGM Atasi Belum Optimalnya Pengelolaan Hutan</h5>

            </div>
          </div>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
    <div class="container-fluidmd bg-1 text-center">
      <h3>Ini adalah Daftar Berita</h3><br>
      <div class="row">
        @foreach($newss as $news)

          <div class="post">
            <a href="#">
              <div class="hover01 column">
                <div>
                  <figure><img src="#" class="img-responsive" height="auto" width="auto" /></figure>
                </div>
              </div>
            </a>

              <h3 class="ctitle">{{ $news->news_title }}</h3>
              <p class="blog-p1" style="margin-top: 6px"><small>Published: {{ date('M j, Y', strtotime($news->created_at)) }}</small></p>

              <p class="blog-p2" style="margin-top: 6px">{{ substr(strip_tags($news->news_content), 0, 300) }}{{ strlen(strip_tags($news->news_content)) > 300 ? "..." : "" }}</p>
              <p class="blog-p3"><a href="#" class="btn-outline" style="margin-top: 6px">Read More</a></p>
          </div>

          <hr>

      @endforeach
      </div>
    </div><br><br>
  </div>
</div>

<footer id="footer">
  <div class="footer_top">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4">
          <div class="single_footer_top wow fadeInRight">
            <h2>About Us</h2>
            <p>To give our best on developing the software of your needs, we are affiliating with PT Exel Sistem Solusindo a software house company that consist of young and energetic team. With years of experiences in IT, we have all the expertise that you need</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer_bottom">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="footer_bottom_left">
            <p>Copyright &copy; 2018 <a href="home.html">Klinik Hasil Hutan</a></p>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="footer_bottom_right">
            <p><a href="http://www.exel-int.com/index.php#about">Developed BY EXES</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<script>
    $(function () {
  $('[data-tooltip="tooltip"]').tooltip()
  });
</script>
<script src="{{ asset('enduser/newslist/js/jquery.min.js')}}"></script>
<script src="{{ asset('enduser/newslist/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('enduser/newslist/js/wow.min.js')}}"></script>
<script src="{{ asset('enduser/newslist/js/slick.min.js')}}"></script>
<script src="{{ asset('enduser/newslist/js/custom.js')}}"></script>

</body>
</html>
