<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('news', function (Blueprint $table) {
          $table->increments('news_id')->unsigned();
          $table->string('news_title', 255)->nullable();
          $table->text('news_content')->nullable();
          $table->text('news_picture_path')->nullable();
          $table->dateTime('created_at');
          $table->string('created_by', 45)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('news');
    }
}
