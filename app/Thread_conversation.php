<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thread_conversation extends Model
{
  protected $table = 'thread_conversations';

  //protected $fillable = ['thread_case_id','comments','created_by'];

  function thread_case(){
    return $this->belongsTo('App\Thread_case');
  }
}
