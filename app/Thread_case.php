<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thread_case extends Model
{
      protected $table = 'thread_cases';
      protected $primaryKey = 'thread_case_id';

//      protected $fillable = ['thread_case_id','thread_status','clinic_category_id','created_by','updated_by','subject','detail','resolution_answer'];

      function clinic_category() {
        return $this->belongsTo('App\Clinic_category');
      }

      function thread_conversations() {
        return $this->belongsTo('App\Thread_conversation');
      }

}
