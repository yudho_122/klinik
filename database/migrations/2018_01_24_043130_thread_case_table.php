<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ThreadCaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('thread_cases', function (Blueprint $table) {
          $table->increments('thread_case_id');
          $table->string('thread_status',45)->nullable();
          $table->integer('clinic_category_id')->unsigned();
          $table->foreign('clinic_category_id')->references('clinic_category_id')->on('clinic_categorys');
          $table->string('created_by',45)->nullable();
          $table->string('updated_by',45)->nullable();
          $table->string('subject',255)->nullable();
          $table->text('detail')->nullable();
          $table->text('resolution_answer')->nullable();
          $table->timestamps();
          $table->integer('additional_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('thread_cases');
    }
}
