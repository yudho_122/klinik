<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use App\User;
use App\User_token;
use App\Group;
use Session;
use Purifier;

class SystemAdminController extends Controller
{
  public function index($email = null) {

    if ($email == null) {
        $user   = DB::table('users')
                    ->join('groups', 'users.group_id', '=', 'groups.group_id')
                    ->orderBy('users.created_at', 'desc')->get();
        return $user;

    } else {
        return $this->show($email);
    }
  }


  public function create(Request $request) {
    $validator 	=	Validator::make($request->all(), [
      'first_name'                =>  'required|max:45',
      'email'                     =>  'required|email|max:45|unique:users',
      'password'                  =>  'required|min:6',
      // 'group_id'                  =>  'required',
    ]);

    if($validator->fails()) {
          $error = $validator->messages()->toJson();

          echo json_encode(['status' => 'salah', 'message' => $error]);
      } else {
          $user =	User::create([
                'email'         =>  $request->input('email'),
                'first_name'    =>  $request->input('first_name'),
                'last_name'     =>  $request->input('last_name'),
                'password'      =>  md5($request->input('password').md5($request->input('email'))),
                'group_id'      => '01-ADMIN',
                'alamat'        =>  $request->input('alamat'),
                'telp'          =>  $request->input('telp'),
                'gcm_id'        =>  $request->input('gcm_id'),
                'user_approval_status'  =>  'APPROVE',
                'id_number'     =>  $request->input('id_number'),

          ]);
        //$email = 'email';
        $date = date('Y-m-d H:i:s');
        $expired_at = date('Y-m-d H:i:s', strtotime('+30min', strtotime($date)));
        $key = md5($request->first_name. md5(strtotime($date)));

        $user_token = User_token::create([
                        'user_email' => $user->email,
                        'user_token' => $key,
                        'created_at' => $date,
                        'expired_at' => $expired_at,
        ]);

          echo json_encode(['status' => 'success', 'message' => $request->all()]);
      }
  }

  public function show($email)
  {
      return User::find($email);
      //return view('news.show')->withNews($news);
  }

  public function edit($email) {
        $user = User::find($email);
        return view('user.editadmin')->with('user', $user);;
  }

  public function getUser(Request $request) {
    $email 	=	$request->input('email');
    $user      =   User::find($email);

      echo json_encode($user);
  }

  public function update(Request $request, $email) {
    //$email  = $request->input('email');
    if (!empty($email)) {
      $validator 	=	Validator::make($request->all(), [
        'first_name'                =>  'required|max:45',
        'password'                  =>  'required|min:6',
        'group_id'                  =>  'required',
      ]);
          if($validator->fails()) {
              $error = $validator->messages()->toJson();
              echo json_encode(['status' => 'salah', 'message' => $error]);
          } else {
                  $user       =   User::find($email);
                  $user->first_name    =  $request->input('first_name');
                  $user->last_name     =  $request->input('last_name');
                  $user->password      =  md5($request->input('password').md5($request->input('email')));
                  $user->alamat        =  $request->input('alamat');
                  $user->group_id      =  $request->input('group_id');
                  $user->telp          =  $request->input('telp');
                  $user->gcm_id        =  $request->input('gcm_id');
                  $user->id_number         =  $request->input('id_number');
                  $result                     =   $user->save();

              //return view('user.daftaradmin');
              echo json_encode(['status' => 'success', 'message' => 'data was updated']);

          }
      } else {
          //return view('user.daftaradmin');
          echo json_encode(['status' => 'false', 'message' => 'No parameter Id selected']);
      }


  }

  public function destroy($email) {
    $user 	=	User::findOrFail($email);
    $response 	=	$user->delete();

    echo json_encode(['status' => 'success', 'message' => 'Data was deleted']);
  }
}
