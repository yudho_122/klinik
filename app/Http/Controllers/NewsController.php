<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Session;
use Purifier;
use Image;

class NewsController extends Controller
{

    public function index($news_id = null) {

      if ($news_id == null) {
          return News::orderBy('news_id', 'asc')->get();
      } else {
          return $this->show($news_id);
      }
       //$newss = News::orderBy('created_at', 'desc')->paginate(10);
       //return view('news.index')->with('newss', $newss);

    }

    public function tampil($news_id) {
      $news = News::find($news_id);
      return view('news.view')->with('news', $news);
    }

    public function create()
    {
        return view('news.index');
    }

    public function store(Request $request) {

        $news = new News;

        $validator = Validator::make($request->all(), [
            'news_title'        => 'required|max:255',
            'news_picture_path' => 'required|mimes:jpeg,png|max:2048',
            'created_by'        => 'required|max:45',
        ]);

        if($validator->fails()) {
            $error = $validator->messages()->toJson();

            echo json_encode(['status' => 'salah', 'message' => $error]);
        } else {
            $file   =   time().'.'.$request->news_picture_path->getClientOriginalExtension();
            $request->news_picture_path->move(public_path('images/news'), $file);
            $path   =   public_path('images/news/'). $file;

            $news                       =   new News;
            $news->news_title           =   $request->input('news_title');
            $news->news_content         =   $request->input('news_content');
            $news->news_picture_path    =   $path;
            $news->created_by           =   $request->input('created_by');
            $result                     =   $news->save();

            echo json_encode(['status' => 'success', 'message' => $request->all()]);
        }

        return 'news record successfully created with id ' . $news->news_id;
        //return redirect()->route('ini.index');
    }

    public function show($news_id)
    {
        return News::find($news_id);
        return view('news.view')->with('news', $news);
    }


    public function getNews(Request $request) {
        $news_id    =   $request->news_id;
        $news       =   News::find($news_id);

        echo json_encode($news);
    }

    public function update(Request $request, $news_id) {

        //$news = News::find($news_id);

        //$news_id    =   $request->input('news_id');
        if (!empty($news_id)) {
            $validator = Validator::make($request->all(), [
                'news_title' => 'required|max:255',
                // 'news_picture_path' => 'required',
                'created_by' => 'required|max:45',
            ]);
            if($validator->fails()) {
                $error = $validator->messages()->toJson();

                echo json_encode(['status' => 'salah', 'message' => $error]);
            } else {
                $news       =   News::find($news_id);
                $news->news_title     =   $request->input('news_title');
                $news->news_content     =   $request->input('news_content');
                $ex_name    =   $news->news_picture_path;
                $new_name   =   substr($ex_name, -14);
                if($request->hasFile('news_picture_path')) {
                    if (File::exists(base_path()) ."/public/images/news/". $new_name) {
                      File::delete(base_path() ."/public/images/news/". $new_name);
                    }
                    $file   =   time().'.'.$request->news_picture_path->getClientOriginalExtension();
                    $request->news_picture_path->move(public_path('images/news'), $file);
                    $path   =   public_path('images/news/'). $file;
                    $news->news_picture_path = $path;
                }
                $news->created_by     =   $request->input('created_by');
                $result     =   $news->save();

                echo json_encode(['status' => 'success', 'message' => 'data was updated']);

                return "Sucess updating news #" . $news->news_id;
            }
        } else {
            echo json_encode(['status' => 'false', 'message' => 'No parameter Id selected']);
        }


        //return redirect()->route('ini.show', $news->news_id);
    }

    public function destroy($id) {
    	$news   =   News::findOrFail($id);
        $filename   =   $news->news_picture_path;
        $rest = substr($filename, -14);
        if (File::exists(base_path()) ."/public/images/news/". $rest) {
          File::delete(base_path() ."/public/images/news/". $rest);
        }
        $response   =   $news->delete();

        echo json_encode(['status' => 'success', 'message' => 'Data was deleted']);

        return view('news.news')->with('news', $news);
    }
}
