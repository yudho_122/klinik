<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SystemSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('sistem_settings', function (Blueprint $table) {
         $table->string('system_setting_key',45)->unique();
         $table->primary('system_setting_key');
         $table->text('system_setting_value');
         $table->dateTime('updated_at');
         $table->string('updated_by', 45)->nullable();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('sistem_settings');
    }
}
