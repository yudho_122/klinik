<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserTokenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('user_tokens', function (Blueprint $table) {
        $table->string('user_email',45)->unique();
        $table->foreign('user_email')->references('email')->on('users');
        $table->text('user_token')->nullable();
        $table->dateTime('created_at')->nullable();
        $table->dateTime('expired_at')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_tokens');
    }
}
