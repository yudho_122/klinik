@extends('admintemplate.layout')

@section('content')
<div class="row">
    <div class="col s12">
        <div class="page-title">Report Page</div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
              <form method="POST" action="{{ route('reportResolution') }}">
              <span class="card-title">Pick a Date</span>
              <div class="row">
                  <div class="col s4">
                      <label for="birthdate">Start Date</label>
                      <input id="dateAwal" name="dateAwal" value="Tanggal Awal" type="text" class="datepicker">
                  </div>
                  <div class="col s4">
                      <label for="birthdate">End Date</label>
                      <input id="dateAkhir" name="dateAkhir" value="tanggal Akhir" type="text" class="datepicker">
                  </div>
                  <div class="col s2">
                      <!-- <td><a href="#" class="waves-effect waves-light btn m-b-xs" style="top: 20px; left: 50px;">Process</a> -->
                      <input type="submit" class="waves-effect waves-light btn m-b-xs" style="top: 20px; left: 50px;"></input>
                  </div>
              </div>
            </form>
            </div>
        </div>
    </div>
</div>

@endsection
