<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="{{ asset('enduser/images/logo.png')}}" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="{{ asset('enduser/resource/css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('../css/font-awesome.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('../css/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('enduser/resource/css/theme.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('enduser/resource/css/style.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('enduser/resource/css/navbar.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('enduser/resource/css/table.css')}}">


  <style>

    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }

    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;

    }
  </style>
</head>
<body>
<div class="container">
      <nav class="navbar navbar-inverse navbar-fixed-left navbar-fixed-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../home.html">
      	  <img src="{{ asset('enduser/images/logo.png')}}" style="width:45px; margin-left: 5px" alt="Image">
      	  </a>
          </div>
          <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
              <li ><a href="../home.html">Home</a></li>
              <li><a href="#">KPI</a></li>
              <li class="active"><a href="#">Resource</a></li>
      		<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Task<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Page 1-1</a></li>
                <li><a href="#">Page 1-2</a></li>
                <li><a href="#">Page 1-3</a></li>
              </ul>
            </li>
            </ul>
      	    <form class="navbar-form navbar-left" action="/action_page.php">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search" name="search">
                 <div class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                  <i class="glyphicon glyphicon-search"></i>
                  </button>
                </div>
              </div>
            </form>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Notification (<b>2</b>)
             <span class="glyphicon glyphicon-bell"></span></a>
              <ul class="dropdown-menu notify-drop">
                <div class="notify-drop-title">
                    <div class="row">
                      <div class="col-md-6 col-sm-6 col-xs-6">Request Pesan (<b>2</b>)</div>
                      <div class="col-md-6 col-sm-6 col-xs-6 text-right"><a href="" class="rIcon allRead" data-tooltip="tooltip" data-placement="bottom" title="tümü okundu."><i class="fa fa-dot-circle-o"></i></a></div>
                    </div>
                </div>
            <div class="drop-content">
              <li>
                <div class="col-md-3 col-sm-3 col-xs-3"><div class="notify-img"><img src="http://placehold.it/45x45" alt=""></div></div>
                <div class="col-md-9 col-sm-9 col-xs-9 pd-l0"><a href="">Comel</a> menyukai sebuah kiriman di <a href="">Dota 2</a> <a href="" class="rIcon"><i class="fa fa-dot-circle-o"></i></a>
                <p class="time">Baru saja</p>
                </div>
              </li>
              <li>
                <div class="col-md-3 col-sm-3 col-xs-3"><div class="notify-img"><img src="http://placehold.it/45x45" alt=""></div></div>
                <div class="col-md-9 col-sm-9 col-xs-9 pd-l0"><a href="">Yudhono</a> menyukai kiriman <a href="">Abdullah</a> <a href="" class="rIcon"><i class="fa fa-dot-circle-o"></i></a>
                <p>Lorem ipsum sit dolor amet consilium.</p>
                <p class="time">1 menit yang lalu</p>
                </div>
              </li>
              <li>
                <div class="col-md-3 col-sm-3 col-xs-3"><div class="notify-img"><img src="http://placehold.it/45x45" alt=""></div></div>
                <div class="col-md-9 col-sm-9 col-xs-9 pd-l0"><a href="">Abdullah</a> mengirim sesuatu ke <a href="">Gan Numpang nanya dong</a> <a href="" class="rIcon"><i class="fa fa-dot-circle-o"></i></a>
                <p>Lorem ipsum sit dolor amet consilium.</p>
                <p class="time">29 menit yang lalu</p>
                </div>
              </li>
              <li>
                <div class="col-md-3 col-sm-3 col-xs-3"><div class="notify-img"><img src="http://placehold.it/45x45" alt=""></div></div>
                <div class="col-md-9 col-sm-9 col-xs-9 pd-l0"><a href="">Ahmet</a> menjawab <a href="">icikiwir</a> <a href="" class="rIcon"><i class="fa fa-dot-circle-o"></i></a>
                <p>HEYO WAZAP MAMEN</p>
                <p class="time">hari ini 13:18</p>
                </div>
              </li>
              <li>
                <div class="col-md-3 col-sm-3 col-xs-3"><div class="notify-img"><img src="http://placehold.it/45x45" alt=""></div></div>
                <div class="col-md-9 col-sm-9 col-xs-9 pd-l0"><a href="">kawkaw</a> menjawab <a href=""> tang ina</a> <a href="" class="rIcon"><i class="fa fa-dot-circle-o"></i></a>
                <p>Lorem ipsum sit dolor amet consilium.</p>
                <p class="time">2 jam yang lalu</p>
                </div>
              </li>
            </div>
            <div class="notify-drop-footer text-center">
              <a href=""><i class="fa fa-eye"></i> View All</a>
            </div>
            </ul>
              <li><a href="../login.html"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>&nbsp;&nbsp;&nbsp;&nbsp;
            </ul>
          </div>
        </div>
      </nav>
      <br>
      <div class="content_top">
        <h3>Data Resource</h3>
      </div>
  <div class="content_middle">

    <div class="table-responsive" ng-app="resourcenyaRecords" ng-controller="resourcesdiController">
       <table id="example" datatable="ng" class="table" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Resource Title</th>
                <th>Resource Description</th>
                <th>Resource Path</th>
                <th>Created At</th>
                <th>Created By</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>Resource Title</th>
              <th>Resource Description</th>
              <th>Resource Path</th>
              <th>Created At</th>
              <th>Created By</th>
            </tr>
        </tfoot>
        <tbody>
            <tr ng-repeat="re in resources">
                <td>@{{re.resource_title}}</td>
                <td>@{{re.resource_description}}</td>
                <td>@{{re.resource_path}}</td>
                <td>@{{re.created_at}}</td>
                <td>@{{re.created_by}}</td>
            </tr>
        </tbody>
    </table>
  </div>
</div>
</li>
<script src="{{ asset('../js/jquery.min.js')}}"></script>
<script src="{{ asset('../js/bootstrap.min.js')}}"></script>
<!-- <script src="{{ asset('../js/custom.js')}}"></script> -->
<script>
    $(function () {
  $('[data-tooltip="tooltip"]').tooltip()
  });
</script>

<script>
$(document).ready(function() {
    $('#example').DataTable();
});
</script>

<script>
$( '.table' ).each(function( i ) {

    var worktable = $(this);
    var num_head_columns = worktable.find('thead tr th').length;
    var rows_to_validate = worktable.find('tbody tr');

    rows_to_validate.each( function (i) {

        var row_columns = $(this).find('td').length;
        for (i = $(this).find('td').length; i < num_head_columns; i++) {
            $(this).append('<td class="hidden"></td>');
        }

    });

});
</script>
<!-- <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js')}}"></script> -->
<script src="{{ asset('admin/assets/plugins/jquery/jquery-2.2.0.min.js')}}"></script>
<script src="{{ asset('app/lib/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular-datatables.min.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="{{ asset('app/controllers/resourceEnduser.js') }}"></script>
</body>
</html>
