<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/sys_admin', function (Request $request) {
    return $request->sys_admin();
});

Route::post('register', 'LoginController@register');
Route::post('login', 'LoginController@login')->name('login');
Route::post('recover', 'LoginController@recover');
Route::group(['middleware' => ['jwt.auth']], function() {
    Route::get('logout', 'LoginController@logout')->name('logout');
    // Route::get('test', function(){
    //     return response()->json(['foo'=>'bar']);
    // });
    Route::get('user', 'LoginController@getAuthUser');
});