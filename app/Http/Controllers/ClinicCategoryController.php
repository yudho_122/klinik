<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\Clinic_category;
use Validator;
use Session;
use Purifier;
use Image;

class ClinicCategoryController extends Controller
{
  public function index($clinic_category_id = null) {

    if ($clinic_category_id == null) {
        return Clinic_category::orderBy('clinic_category_id', 'asc')->get();
    } else {
        return $this->show($clinic_category_id);
    }
  }

    public function store(Request $request) {

      $clinic_category = new Clinic_category;

    	$validator 	=	Validator::make($request->all(), [
    		'clinic_category_name' 	=>	'required|max:45',
    		'clinic_category_des' 	=>	'required',
    		'clinic_category_image' => 'required|mimes:jpeg,png|max:2024',
    		'created_by' 			=> 'required',
    	]);

    	if($validator->fails()) {
            $error = $validator->messages()->toJson();

            echo json_encode(['status' => 'Error', 'message' => $error]);
        } else {
            $file   =   time().'.'.$request->clinic_category_image->getClientOriginalExtension();
            $request->clinic_category_image->move(public_path('images/category'), $file);
            $path   =   public_path('images/category/'). $file;

            $clinic 			=	new Clinic_category;
            $clinic->clinic_category_name 	=	$request->input('clinic_category_name');
            $clinic->clinic_category_des 	=	$request->input('clinic_category_des');
            $clinic->clinic_category_image 	=	$path;
            //$clinic->created_at 			=	date('Y-m-d H:i:s');
            $clinic->created_by 			=	$request->input('created_by');
            $clinic->save();

            echo json_encode(['status' => 'success', 'message' => $request->all()]);
        }
    }

    public function tampil($clinic_category_id) {
      $clinic_category = Clinic_category::find($clinic_category_id);
      return view('clinic_category.view')->with('clinic_category', $clinic_category);
    }

    public function show($clinic_category_id){
      return Clinic_category::find($clinic_category_id);

      return view('clinic_category.view')->with('clinic_category', $clinic_category);
    }

    public function getClinic(Request $request) {
    	$clinic_category_id 	=	$request->$clinic_category_id;
    	$clinic 	=	Clinic_category::find($clinic_category_id);

    	echo json_encode(['status' => 'success', 'message' => $clinic]);
    }

    public function update(Request $request, $clinic_category_id) {
    //	$clinic_category_id 	=	$request->input('clinic_id');
    	if (!empty($clinic_category_id)) {
            $validator 	=	Validator::make($request->all(), [
	    		'clinic_category_name' 	=>	'required|max:45',
	    		'clinic_category_des' 	=>	'required',
	    		//'clinic_category_image' => 'required|mimes:jpeg,png|max:2024',
	    		'created_by' 			      => 'required',
	    	]);
            if($validator->fails()) {
                $error = $validator->messages()->toJson();

                echo json_encode(['status' => 'salah', 'message' => $error]);
            } else {
                $clinic       =   Clinic_category::find($clinic_category_id);
                $clinic->clinic_category_name 	=	$request->input('clinic_category_name');
            	$clinic->clinic_category_des 	=	$request->input('clinic_category_des');
                $ex_name    =   $clinic->clinic_category_image;
                $new_name   =   substr($ex_name, -14);
                if($request->hasFile('clinic_category_image')) {
                    if (File::exists(base_path()) ."/public/images/category/". $new_name) {
                      File::delete(base_path() ."/public/images/category/". $new_name);
                    }
                    $file   =   time().'.'.$request->clinic_category_image->getClientOriginalExtension();
                    $request->clinic_category_image->move(public_path('images/category'), $file);
                    $path   =   public_path('images/category/'). $file;
                    $clinic->clinic_category_image = $path;
                }
                $clinic->created_by     =   $request->input('created_by');
                $result                 =   $clinic->save();

                echo json_encode(['status' => 'success', 'message' => 'data was updated']);
            }
        } else {
            echo json_encode(['status' => 'false', 'message' => 'No parameter Id selected']);
        }
    }

    public function destroy($id) {
    	$clinic 	=	Clinic_category::findOrFail($id);
    	$filename   =   $clinic->clinic_category_image;
        $rest = substr($filename, -14);
        if (File::exists(base_path()) ."/public/images/category/". $rest) {
          File::delete(base_path() ."/public/images/category/". $rest);
        }
        $response   =   $clinic->delete();

        echo json_encode(['status' => 'success', 'message' => 'Data was deleted']);
    }
}
