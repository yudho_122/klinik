var app = angular.module('groupRecords', ['datatables'])
        .constant('API_URL', 'http://localhost/klinik-hutan/public/api/v1/');

app.controller('groupsController', function($scope, $timeout ,$http, $window, API_URL) {
    //retrieve employees listing from API
    $window.location;

  //  $scope.groups=[];
    $http.get(API_URL + "group")
        .success(function(response) {
                $scope.group = response;
            });
            //show modal form
   $scope.toggle = function(modalstate, id) {
       $scope.modalstate = modalstate;

       switch (modalstate) {
           case 'add':
               $scope.group = "";
               $scope.form_title = "Add New Group";
               break;
           case 'init':
               $('#modal-trigger-' + id).leanModal();
           break;
           case 'edit':
               $scope.form_title = "Group Detail";
               $scope.group_id = id;

               $http.get(API_URL + 'group/' + id)
                       .success(function(response) {
                           console.log(response);

                           $('label[for*="group_id"], label[for*="group_name"], label[for*="group_des"], label[for*="created_by"]').addClass( 'active' );

                           $scope.group.group_id = response.group_id;
                           $scope.group.group_name = response.group_name;
                           $scope.group.group_des = response.group_des;
                           $scope.group.created_by = response.created_by;

                       });
               break;
           default:
               break;
       }
  }

  var formData = new FormData();

  // save mode_edit
  $scope.edit = function(modalstate, id, group_name, group_des, created_by) {
      var url = API_URL + "group/" + id;

      formData.append('group_id', id);
      formData.append('group_name', group_name);
      formData.append('group_des', group_des);
      formData.append('created_by', created_by);

      //append employee id to the URL if the form is in edit mode
        $http({
          method: 'POST',
          url: url,
          data: formData,
          headers: {'Content-Type': undefined}
      }).success(function(response) {
          console.log(response);
          location.reload();
      }).error(function(response) {
          console.log(response);
          alert('This is embarassing. An error has occured. Please check the log for details');
      });
  }

   //save new record / update existing record
   $scope.save = function(modalstate, id) {
       var url = API_URL + "group";

       //append employee id to the URL if the form is in edit mode
         $http({
           method: 'POST',
           url: url,
           data: $.param($scope.group),
           headers: {'Content-Type': 'application/x-www-form-urlencoded'}
       }).success(function(response) {
           console.log(response);
           location.reload();
       }).error(function(response) {
           console.log(response);
           alert('This is embarassing. An error has occured. Please check the log for details');
       });
   }

   //delete record
   $scope.confirmDelete = function(id) {
       var isConfirmDelete = confirm('Are you sure you want this record?');
       if (isConfirmDelete) {
           $http({
               method: 'DELETE',
               url: API_URL + 'group/delete/' + id
           }).
                   success(function(data) {
                       console.log(data);
                       location.reload();
                   }).
                   error(function(data) {
                       console.log(data);
                       alert('Unable to delete');
                   });
       } else {
           return false;
       }
   }

});
