@extends('admintemplate.layout')

@section('content')
<br>

<div class="row" ng-app="threadRecords" ng-controller="threadsController">
    <div class="col s12">
        <div class="page-title">Master Data</div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">

                <span class="card-title">THREAD</span>
                <!-- <p>This is the news list that has been written all the time</p><br> -->
                {{ csrf_field() }}
              <div class="input-field col s12">
                <input id="subject" class="validate" name="subject" type="text" length="255"
                ng-model="thread.subject">
                <label for="subject">subject</label>
              </div>

              <div class="input-field col s12">
                <select ng-model="thread.clinic_category_id">
                  <option value="" disabled selected>Choose Thread Category</option>
                @foreach($clinic_categorys as $clinic_category)
                  <option value="{{ $clinic_category->clinic_category_id }}">{{ $clinic_category->clinic_category_name }}</option>
                @endforeach
                  <label>select category</label>
                </select>
              </div>

              <div class="input-field col s12">
                <textarea id="detail" name="detail" class="materialize-textarea" ng-model="thread.detail"></textarea>
                <label for="detail">Thread Detail</label>
              </div>

              <div class="input-field col s12">
                <input id="created_by" name="created_by" type="text" class="validate" length="255"
                ng-model="thread.created_by">
                <label class="">created by</label>
              </div>
              <button type="button" class="waves-effect waves-light btn blue m-b-xs" id="btn-save" ng-click="save(modalstate, thread.thread_case_id, thread.subject, thread.clinic_category_id, thread.detail, thread.created_by)">Save</button>
              <button type="button" class="waves-effect waves-light btn red m-b-xs" ng-click="toIndex()">Cancel</button>
            </div>
        </div>
    </div>
</div>

@endsection
