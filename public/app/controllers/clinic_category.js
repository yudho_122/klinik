var app = angular.module('clinicCategoryRecords', ['datatables'])
        .constant('API_URL', 'http://localhost/klinik-hutan/public/api/v1/');

app.controller('clinicCategoryController', function($scope, $timeout ,$http, $window, API_URL) {
    //retrieve employees listing from API
    $scope.files = [];
    $scope.errors = [];
    $window.location;

    $http.get(API_URL + "clinic")
            .success(function(response) {
                $scope.clinic_category = response;
            });

    //show modal form
    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.clinic_category = "";
                $scope.form_title = "Add New Category";
                break;
            case 'init':
              $('#modal-trigger-' + id).leanModal();
            break;
            case 'edit':
              $scope.form_title = "Category Detail";
              $scope.clinic_category_id = id;

              $http.get(API_URL + 'clinic/' + id)
                      .success(function(response) {
                          console.log(response);

                          //-- set form to focus
                          $('label[for*="clinic_category_name"], label[for*="clinic_category_des"], label[for*="created_by"]').addClass( 'active' );

                          $scope.clinic_category.clinic_category_name = response.clinic_category_name;
                          $scope.clinic_category.clinic_category_des  = response.clinic_category_des;
                          $scope.clinic_category.created_by           = response.created_by;
                      });

                break;
            default:
                break;
        }

    }

    //-- instansiasi object formData
    var formData = new FormData();

    $scope.setTheFiles = function ($files) {
        angular.forEach($files, function (value, key) {
            formData.append('clinic_category_image', value);
        });
    };


    $scope.edit = function(modalstate, id, clinic_category_name, clinic_category_des, created_by){
        var url = API_URL + "clinic/" + id;

        formData.append('clinic_category_name', clinic_category_name);
        formData.append('clinic_category_des', clinic_category_des);
        formData.append('created_by', created_by);

        $http({
            method: 'POST',
            url: url,
            data: formData,
            headers: {'Content-Type': undefined},
        }).success(function(response) {
            console.log(response);
            location.reload();
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    };

    //save new record / update existing record
    $scope.save = function(modalstate, id, clinic_category_name, clinic_category_des, created_by) {
        var url = API_URL + "clinic";

        //append employee id to the URL if the form is in edit mode
        if (modalstate === 'edit' && modalstate === 'init'){
            url += "/" + id;
        }

        formData.append('clinic_category_name', clinic_category_name);
        formData.append('clinic_category_des', clinic_category_des);
        formData.append('created_by', created_by);

        $http({
            method: 'POST',
            url: url,
            data: formData,
            headers: {'Content-Type': undefined},
        }).success(function(response) {
            console.log(response);
            location.reload();
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

    //redirect ke /news alias news_index
    $scope.redirectIndex = function() {
          $window.location.href = '/clinic';
    }

    //delete record
    $scope.confirmDelete = function(id) {
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'clinic/' + id
            }).
                    success(function(data) {
                        console.log(data);
                        //location.reload();
                        $window.location.href = '/clinic';
                        //$window.location.href;

                    }).
                    error(function(data) {
                        console.log(data);
                        alert('Unable to delete');
                    });
        } else {
            return false;
        }

    }
});

app.directive('ngFiles', ['$parse', function ($parse) {

    function file_links(scope, element, attrs) {
        var onChange = $parse(attrs.ngFiles);
        element.on('change', function (event) {
            onChange(scope, {$files: event.target.files});
        });
    }

    return {
        link: file_links
    }
}]);
