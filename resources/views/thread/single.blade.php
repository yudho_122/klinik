@extends('admintemplate.layout')

@section('content')

	<div class="row" ng-app="commentRecords" ng-controller="commentController">
		<div class="col s8">
			<h1>INI NANTI JUDUL</h1>
			<p><img src="#" style="max-height:100px;max-width:100px;margin-top:10px;"></p>
				<p class="lead">INI KONTEN NYA</p>
		</div>

      <div class="col s4">
        <div class="card white">
          <dl class="card-content">
						<h4>Komentar</h4>

						<label>Thread Id</label>
            <input id="thread_id" class="validate thread_id" name="thread_id" type="text" length="45"
						ng-model="comment.thread_id" value="">

            <label>Name</label>
            <input id="created_by" class="validate created_by" name="created_by" type="text" length="45"
						ng-model="comment.created_by">

						<label>Komentar</label>
						<textarea id="comment" name="comment" class="materialize-textarea" ng-model="comment.comment"></textarea>
          </dl>
        <hr>
          <div class="row">
            <div class="col s12 m6 l8">
						<button type="button" class="waves-effect waves-light btn blue m-b-xs" id="btn-save" style="width: 323px;" ng-click="addComment(comment.thread_id, comment.comment, comment.created_by)">Tambahkan Komentar</button>
          </div>
        </div>
        <div class="row">

        </div>
      </div>
      </div>
    </div>

@endsection
