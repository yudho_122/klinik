var app = angular.module('resourceRecords', ['datatables'])
        .constant('API_URL', 'http://localhost/klinik-hutan/public/api/v1/');

app.controller('resourcesController', function($scope, $timeout ,$http, $window, API_URL) {
    //retrieve employees listing from API
    $scope.files = [];
    $scope.errors = [];
    $window.location;

    $http.get(API_URL + "resource")
            .success(function(response) {
                $scope.resource = response;
            });

    //show modal form
    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.resource = "";
                $scope.form_title = "Add New Resource";
                break;
            case 'init':
              $('#modal-trigger-' + id).leanModal();
            break;
            case 'edit':
              $scope.form_title = "Resource Detail";
              $scope.resource_id = id;

              $http.get(API_URL + 'resource/' + id)
                      .success(function(response) {
                          console.log(response);

                          //-- set form to focus
                          $('label[for*="resource_title"], label[for*="resource_description"], label[for*="created_by"]').addClass( 'active' );

                          $scope.resource.resource_title       = response.resource_title;
                          $scope.resource.resource_description = response.resource_description;
                          $scope.resource.created_by           = response.created_by;
                      });

                break;
            default:
                break;
        }

    }

    //-- instansiasi object formData
    var formData = new FormData();

    $scope.setTheFiles = function ($files) {
        angular.forEach($files, function (value, key) {
            formData.append('resource_path', value);
        });
    };


    $scope.edit = function(modalstate, id, resource_title, resource_description, created_by){
        var url = API_URL + "resource/" + id;

        formData.append('resource_title', resource_title);
        formData.append('resource_description', resource_description);
        formData.append('created_by', created_by);

        $http({
            method: 'POST',
            url: url,
            data: formData,
            headers: {'Content-Type': undefined},
        }).success(function(response) {
            console.log(response);
            location.reload();
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    };

    //save new record / update existing record
    $scope.save = function(modalstate, id, resource_title, resource_description, created_by) {
        var url = API_URL + "resource";

        //append employee id to the URL if the form is in edit mode
        if (modalstate === 'edit' && modalstate === 'init'){
            url += "/" + id;
        }

        formData.append('resource_title', resource_title);
        formData.append('resource_description', resource_description);
        formData.append('created_by', created_by);

        $http({
            method: 'POST',
            url: url,
            data: formData,
            headers: {'Content-Type': undefined},
        }).success(function(response) {
            console.log(response);
            location.reload();
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

    //redirect ke /news alias news_index
    $scope.redirectIndex = function() {
          $window.location.href = '/resource';
    }

    //delete record
    $scope.confirmDelete = function(id) {
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'resource/' + id
            }).
                    success(function(data) {
                        console.log(data);
                        //location.reload();
                        $window.location.href = '/resource';
                        //$window.location.href;

                    }).
                    error(function(data) {
                        console.log(data);
                        alert('Unable to delete');
                    });
        } else {
            return false;
        }

    }
});

app.directive('ngFiles', ['$parse', function ($parse) {

    function file_links(scope, element, attrs) {
        var onChange = $parse(attrs.ngFiles);
        element.on('change', function (event) {
            onChange(scope, {$files: event.target.files});
        });
    }

    return {
        link: file_links
    }
}]);
