<?php

namespace App\Http\Controllers;

use App\Resource;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Validator;

class ResourceController extends Controller
{
    public function index($resource_id = null) {
          if ($resource_id == null) {
              return Resource::orderBy('resource_id', 'asc')->get();
          } else {
              return $this->show($resource_id);
          }
    }

    public function tampil($resource_id) {
      $resource = Resource::find($resource_id);
      return view('resource.view')->with('resource', $resource);
    }

    public function show($resource_id){
        return Resource::find($resource_id);
        return view('resource.view')->with('resource', $resource);
    }

    public function store(Request $request) {

      $resource = new Resource;

    	$validator = Validator::make($request->all(), [
            'resource_title'        =>	'required|max:45',
            'resource_description' 	=>	'required',
            'resource_path'         => 'required|mimes:jpeg,png,doc,docx,pdf,xls,pptx,ppt,odt|max:5120',
            //'created_at' 	          =>	'required',
            'created_by'            => 'required|max:45',
        ]);

        if($validator->fails()) {
            $error = $validator->messages()->toJson();

            echo json_encode(['status' => 'salah', 'message' => $error]);
        } else {
            $file   =   time().'.'.$request->resource_path->getClientOriginalExtension();
            $request->resource_path->move(public_path('file/resource'), $file);
            $path   =   public_path('file/resource/'). $file;

            $resource 			=   new Resource;
            $resource->resource_title	=	$request->input('resource_title');
            $resource->resource_description 	=	$request->input('resource_description');
            $resource->resource_path 			=	$path;
            //$resource->created_at 				=	date('Y-m-d H:i:s');
            $resource->created_by 				=	$request->input('created_by');
            $result 							        =	$resource->save();

            echo json_encode(['status' => 'success', 'message' => $request->all()]);
        }
    }

    public function getResource(Request $request) {
    	$resource_id 	=	$request->input('resource_id');
    	$resource 		=	Resource::find($resource_id);

    	echo json_encode($resource);
    }

    public function update(Request $request, $resource_id) {
    	//$resource_id 	=	$request->input('resource_id');
    	if (!empty($resource_id)) {
            $validator = Validator::make($request->all(), [
	            'resource_title'        =>	'required|max:45',
	            'resource_description' 	=>	'required',
	            //'resource_path'         => 'required|mimes:jpeg,png|max:5120',
	            'created_by'            => 'required|max:45',
	        ]);
            if($validator->fails()) {
                $error = $validator->messages()->toJson();

                echo json_encode(['status' => 'salah', 'message' => $error]);
            } else {
                $resource       =   Resource::find($resource_id);
                $resource->resource_title	=	$request->input('resource_title');
            	$resource->resource_description 	=	$request->input('resource_description');
                $ex_name    =   $resource->resource_path;
                $new_name   =   substr($ex_name, -14);
                if($request->hasFile('resource_path')) {
                    if (File::exists(base_path()) ."/public/file/resource/". $new_name) {
                      File::delete(base_path() ."/public/file/resource/". $new_name);
                    }
                    $file   =   time().'.'.$request->resource_path->getClientOriginalExtension();
                    $request->resource_path->move(public_path('file/resource'), $file);
                    $path   =   public_path('file/resource/'). $file;
                    $resource->resource_path = $path;
                }
                //$resource->created_at =	date('Y-m-d H:i:s');

            	  $resource->created_by 	=	$request->input('created_by');
            	  $result 							=	$resource->save();

                echo json_encode(['status' => 'success', 'message' => 'data was updated']);
            }
        } else {
            echo json_encode(['status' => 'false', 'message' => 'No parameter Id selected']);
        }
    }

    public function destroy($id) {
    	$resource 	=	Resource::findOrFail($id);
    	$filename   =   $resource->resource_path;
        $rest = substr($filename, -14);
        if (File::exists(base_path()) ."/public/file/resource/". $rest) {
          File::delete(base_path() ."/public/file/resource/". $rest);
        }
    	$response 	=	$resource->delete();

    	echo json_encode(['status' => 'success', 'message' => 'Data was delected']);

      //return view('resource.index')->with('resource', $resource);
    }
}
