@extends('admintemplate.layout')

@section('content')
<div class="row" ng-app="userRecords" ng-controller="userController">
    <div class="col s12">
        <div class="page-title">Master Data</div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <span class="card-title">Administrator Data</span>
                <!-- <p>This is the news list that has been written all the time</p><br> -->
                <button class="waves-effect waves-light btn blue m-b-xs modal-trigger" href="#modal1" ng-click="toggle('add', 0)"><i class="material-icons left">add_box</i>Tambah</button>
                <table id="example" datatable="ng" class="display responsive-table datatable-example">
                  <thead>
                      <tr>
                          <th>Email</th>
                          <th>Name</th>
                          <th>Group</th>
                          <th>Status</th>
                          <th>Id Number</th>
                          <th></th>
                      </tr>
                  </thead>
                  <tfoot>
                      <tr>
                        <th>Email</th>
                        <th>Name</th>
                        <th>Group</th>
                        <th>Status</th>
                        <th>Id Number</th>
                        <th></th>
                      </tr>
                  </tfoot>
                  <tbody>
                      <tr ng-repeat="use in user">
                          <td>@{{ use.email }}</td>
                          <td>@{{ use.first_name }}  @{{ use.last_name}}</td>
                          <td>@{{ use.group_name}}</td>
                          <td>@{{ use.user_approval_status }}</td>
                          <td>@{{ use.id_number }}</td>
                          <td style="width: 30%;">
                          <button class="waves-effect waves-light btn blue m-b-xs modal-trigger" style="bottom: 10px; left: 0.5px; top: 5px;" href="#" ng-click="toEdit(use.email)"><i class="material-icons">mode_edit</i></button>
                          <button class="btn red" style="bottom: 10px; left: 200px;" ng-click="confirmDelete(use.email)"><i class="material-icons">delete</i></button>

                              <!-- edit modal -->
                              <!-- end edit modal -->
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- ini modal buat create -->
    <div id="modal1" class="modal">
      <div class="modal-content">
          <h4>@{{form_title}}</h4>

            {{ csrf_field() }}
          <div class="input-field col s12">
            <input id="email" name="email" type="text" class="validate" length="45"
            ng-model="user.email">
            <label for="email">Email</label>
          </div>

          <div class="input-field col s12">
            <input id="first_name" name="first_name" type="text" class="validate" length="45"
            ng-model="user.first_name">
            <label for="first_name" class="">First Name</label>
          </div>

          <div class="input-field col s12">
            <input id="last_name" name="last_name" type="text" class="validate" length="45"
            ng-model="user.last_name">
            <label for="last_name" class="">Last Name</label>
          </div>

          <div class="input-field col s12">
            <input id="password" name="password" type="text" class="validate"
             ng-model="user.password" >
            <label for="password" class="">Password</label>
          </div>

          <div class="input-field col s12">
            <input id="telp" name="telp" type="text" class="validate" length="20"
             ng-model="user.telp">
            <label for="telp" class="">Telefon</label>
          </div>

          <div class="input-field col s12">
            <input id="gcm_id" name="telp" type="text" class="validate"
             ng-model="user.gcm_id">
            <label for="gcm_id" class="">ID GCM</label>
          </div>

          <div class="input-field col s12">
            <input id="id_number" name="id_number" type="text" class="validate"
             ng-model="user.id_number" >
            <label for="id_number" class="">ID Number</label>
          </div>

          <div class="input-field col s12">
            <textarea id="alamat" name="alamat" class="materialize-textarea" ng-model="group.alamat"></textarea>
            <label for="alamat">Alamat</label>
          </div>
     </div>

      <div class="modal-footer">
         <button type="button" class="modal-action modal-close waves-effect waves-blue btn-flat" id="btn-save" ng-click="save(modalstate, use.email)">Save</button>
          <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">cancel</a>
      </div>
</div>

@endsection
