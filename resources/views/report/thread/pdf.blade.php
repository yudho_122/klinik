<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>

  <body>

    <table width=100%>
      <tr>
        <td align="right" width=20%><img src="enduser/images/logo.png" height="75" width="75"></td>
        <td align="center" width=70%><h3 style="text-align: center;"><strong>KEMENTRIAN LINGKUNGAN HIDUP DAN KEHUTANAN
        <br>BADAN PENELITIAN, PENGEMBANGAN DAN INOVASI</strong></h3></td>
        <td align="right" width=5%></td>
      </tr>
    </table>
    <hr size=3>
    <p style="text-align: center;"><strong>LAPORAN JUMLAH KASUS</strong></p>
    <p style="text-align: center;"><strong>PERIODE : {{$start}} sampai {{$end}}</strong></p>
    <p>&nbsp;</p>
    <table style=" border-collapse: collapse; border-color: black; margin-left: auto; margin-right: auto;" border="2" width=100%>
      <thead>
          <tr>
              <th align="center">Subject</th>
              <th align="center">Category</th>
              <th align="center">Created At</th>
              <th align="center">Created By</th>
              <th align="center">Jumlah Comment</th>
              <th align="center">Status Thread</th>
          </tr>
      </thead>
      <tbody>
        @foreach ($report as $repor)
        <tr style="height: 28px">
          <td align="center" style="height: 28px;">{{$repor->subject}}</td>
          <td align="center" style="height: 28px;">{{$repor->clinic_category_name}}</td>
          <td align="center" style="height: 28px;">{{$repor->created_at}}</td>
          <td align="center" style="height: 28px;">{{$repor->created_by}}</td>
          <td align="center" style="height: 28px;">{{$repor->comment}}</td>
          <td align="center" style="height: 28px;">{{$repor->thread_status}}</td>
        </tr>
        @endforeach
      </tbody>
    <table>
    </body>
</html>
