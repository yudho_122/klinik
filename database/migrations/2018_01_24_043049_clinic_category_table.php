<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClinicCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('clinic_categorys', function (Blueprint $table) {
         $table->increments('clinic_category_id');
         $table->string('clinic_category_name', 100);
         $table->text('clinic_category_des')->nullable();
         $table->text('clinic_category_image')->nullable();
         $table->dateTime('created_at');
         $table->string('created_by', 45)->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('clinic_categorys');
    }
}
