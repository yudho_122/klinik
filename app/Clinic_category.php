<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clinic_category extends Model
{
    protected $primaryKey = 'clinic_category_id';
    protected $table = 'clinic_categorys';
    //public $timestamps = false;

    //protected $fillable = array('clinic_category_id', 'clinic_category_name', 'clinic_category_des','clinic_category_image','created_at','created_by');

    public function setUpdatedAt($value){
          return $this;
    }

    public function thread_cases() {
      return $this->hasMany('app\Thread_case');
    }
}
