@extends('admintemplate.layout')

@section('content')
<div class="row">
    <div class="col s12">
        <div class="page-title">Report Page</div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
              <form method="POST">
              <span class="card-title">Pick a Date</span>
              <div class="row">
                  <div class="col s3">
                      <label for="birthdate">Start Date</label>
                      <input id="dateAwal" name="dateAwal" value="Tanggal Awal" type="text" class="datepicker">
                  </div>
                  <div class="col s3">
                      <label for="birthdate">End Date</label>
                      <input id="dateAkhir" name="dateAkhir" value="Tanggal Akhir" type="text" class="datepicker">
                  </div>
                  <div class="col s3">
                    <label>Select Category</label>
                    <select name="clinic" id="clinic">
                      <option disabled selected>Choose Thread Category</option>
                      <option value="all">Select All</option>
                      @foreach ($clinic_categorys as $clinic_category)
                      <option value="{{ $clinic_category->clinic_category_id }}">{{ $clinic_category->clinic_category_name }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col s2">
                      <!-- <td><a href="#" class="waves-effect waves-light btn m-b-xs" style="top: 20px; left: 50px;">Process</a> -->
                    <button type="Submit" href="{{route('reportThread')}}" name="submit" value="noPDF" class="waves-effect waves-light btn m-b-xs" style="top: 20px; left: 50px;">Submit</button>
                  </div>
              </div>
            </form>

            <button type="button" id="getPdf" name="button" class="waves-effect waves-light btn red m-b-xs " style="float: right;">Save as PDF</button>

            <form method="GET" action="{{route('reportThread')}}">
            <span class="card-title">Report from <b id="awal">{{ $start }}</b> until <b id="akhir">{{ $end }}</b></span>
            <span class="card-title">From <b id="namaclinic">{{ $clinicName }}</b> Category</span>
            <table class="striped">
                <thead>
                    <tr>
                        <th>Subject</th>
                        <th>Category</th>
                        <th>Created At</th>
                        <th>Created By</th>
                        <th>Jumlah Comment</th>
                        <th>Status Thread</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach ($report as $repor)
                    <tr>
                        <td>{{$repor->subject}}</td>
                        <td>{{$repor->clinic_category_name}}</td>
                        <td>{{$repor->created_at}}</td>
                        <td>{{$repor->created_by}}</td>
                        <td>{{$repor->comment}}</td>
                        <td>{{$repor->thread_status}}</td>
                    </tr>
                    @endforeach
                    <tr>

                    </tr>
                  </tbody>
            </table>
            <br>

                  <br><br>
              <!-- <input type="submit" class="waves-effect waves-light btn m-b-xs" style="top: 20px; left: 50px;"></input> -->
          </form>
        </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $("#getPdf").click(function() {
            var dateAwal = $("#awal").html();
            var dateAkhir = $("#akhir").html();
            var clinic = $("#namaclinic").html();
            alert(clinic);
            $('.lean-overlay').remove();
            $.ajax({
                type : 'POST',
                url  : "{{ route('downloadThread') }}",
                data : {'dateAwal' : dateAwal, 'dateAkhir' : dateAkhir},
                success: function(response) {
                  console.log(response);
                }
            });
        });
    </script>
@endsection
