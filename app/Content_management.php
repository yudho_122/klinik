<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content_management extends Model
{
  protected $table = 'content_managements';
  protected $primaryKey = 'content_management_id';
  public $timestamps = false;

 //protected $fillable = ['content_management_id','content_management_type','content_management_path','content_management_lat',
 //'content_management_long','thread_case_id'];
  public thread_case(){
    return $this->belongsTo('App\Thread_case', 'thread_case_id', 'thread_case_id');
  }
}
