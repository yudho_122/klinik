@extends('admintemplate.layout')

@section('content')
<div class="row">
    <div class="col s12">
        <div class="page-title">Master Data</div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <span class="card-title">Administrator</span>
                <!-- <p>This is the news list that has been written all the time</p><br> -->
                <button class="waves-effect waves-light btn m-b-xs modal-trigger" href="#modal1"><i class="material-icons left">add_box</i>Tambah</button>
                <div id="modal1" class="modal">
                    <div class="modal-content">
                      <form>
                        <h4>Create New Administrator</h4>
                              <div class="input-field">
                                  <input id="first_name" type="text" class="validate">
                                  <label for="first_name">Email</label>
                              </div>
                              <div class="input-field">
                                  <input id="first_name" type="text" class="validate">
                                  <label for="first_name">First Name</label>
                              </div>
                              <div class="input-field">
                                  <input id="first_name" type="text" class="validate">
                                  <label for="first_name">Last Name</label>
                              </div>
                              <div class="input-field">
                                  <input id="first_name" type="text" class="validate">
                                  <label for="first_name">Password</label>
                              </div>
                              <div class="input-field">
                                  <input id="first_name" type="text" class="validate">
                                  <label for="first_name">Group</label>
                              </div>
                              <div class="input-field">
                                  <input id="first_name" type="text" class="validate">
                                  <label for="first_name">Alamat</label>
                              </div>
                              <div class="input-field">
                                  <input id="first_name" type="text" class="validate">
                                  <label for="first_name">Telp</label>
                              </div>
                              <div class="input-field">
                                  <input id="first_name" type="text" class="validate">
                                  <label for="first_name">GCM_ID</label>
                              </div>
                    </form>
                    </div>
                    <div class="modal-footer">
                      <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Save</a>
                      <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
                    </div>
              </div>
                <table id="example" class="display responsive-table datatable-example">
                    <thead>
                        <tr>
                            <th>Email</th>
                            <th>Nama</th>
                            <th>Telepon</th>
                            <th>Gcm_id</th>
                            <th>Create_at</th>
                            <th align="center">Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                          <th>Email</th>
                          <th>Nama</th>
                          <th>Telepon</th>
                          <th>Gcm_id</th>
                          <th>Create_at</th>
                          <th align="center">Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <tr>
                            <td>oi@gmail.com</td>
                            <td>oi</td>
                            <td>0000</td>
                            <td>1763672</td>
                            <td>Hari ini</td>
                            <td>
                              <button class="waves-effect waves-light btn m-b-xs modal-trigger indigo" href="#modal2"><i class="material-icons">info</i></button>
                              <!--<button class="waves-effect waves-light btn indigo m-b-xs modal-triggers" href="#modal2">View</button>-->
                                  <div id="modal2" class="modal">
                                  <div class="modal-content">
                                    <form>
                                      <h4>View Administrator</h4>
                                            <div class="input-field">
                                                <input id="first_name" type="text" class="validate">
                                                <label for="first_name">Email</label>
                                            </div>
                                            <div class="input-field">
                                                <input id="first_name" type="text" class="validate">
                                                <label for="first_name">First Name</label>
                                            </div>
                                            <div class="input-field">
                                                <input id="first_name" type="text" class="validate">
                                                <label for="first_name">Last Name</label>
                                            </div>
                                            <div class="input-field">
                                                <input id="first_name" type="text" class="validate">
                                                <label for="first_name">Password</label>
                                            </div>
                                            <div class="input-field">
                                                <input id="first_name" type="text" class="validate">
                                                <label for="first_name">Group</label>
                                            </div>
                                            <div class="input-field">
                                                <input id="first_name" type="text" class="validate">
                                                <label for="first_name">Alamat</label>
                                            </div>
                                            <div class="input-field">
                                                <input id="first_name" type="text" class="validate">
                                                <label for="first_name">Telp</label>
                                            </div>
                                            <div class="input-field">
                                                <input id="first_name" type="text" class="validate">
                                                <label for="first_name">GCM_ID</label>
                                            </div>
                                  </form>
                                </div>
                                  <div class="modal-footer">
                                    <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Save</a>
                                    <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
                                  </div>
                            </div>
                            <button class="waves-effect waves-light btn m-b-xs modal-trigger blue" href="#modal3"><i class="material-icons">mode_edit</i></button>
                            <!--<button class="waves-effect waves-light btn indigo m-b-xs modal-triggers" href="#modal2">View</button>-->
                                <div id="modal3" class="modal">
                                <div class="modal-content">
                                  <form>
                                    <h4>Edit New Administrator</h4>
                                          <div class="input-field">
                                              <input id="first_name" type="text" class="validate">
                                              <label for="first_name">Email</label>
                                          </div>
                                          <div class="input-field">
                                              <input id="first_name" type="text" class="validate">
                                              <label for="first_name">First Name</label>
                                          </div>
                                          <div class="input-field">
                                              <input id="first_name" type="text" class="validate">
                                              <label for="first_name">Last Name</label>
                                          </div>
                                          <div class="input-field">
                                              <input id="first_name" type="text" class="validate">
                                              <label for="first_name">Password</label>
                                          </div>
                                          <div class="input-field">
                                              <input id="first_name" type="text" class="validate">
                                              <label for="first_name">Group</label>
                                          </div>
                                          <div class="input-field">
                                              <input id="first_name" type="text" class="validate">
                                              <label for="first_name">Alamat</label>
                                          </div>
                                          <div class="input-field">
                                              <input id="first_name" type="text" class="validate">
                                              <label for="first_name">Telp</label>
                                          </div>
                                          <div class="input-field">
                                              <input id="first_name" type="text" class="validate">
                                              <label for="first_name">GCM_ID</label>
                                          </div>
                                </form>
                              </div>
                                <div class="modal-footer">
                                  <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Save</a>
                                  <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
                                </div>
                           </div>
                            <button class="waves-effect waves-light btn m-b-xs modal-trigger red" href="#modal4"><i class="material-icons">delete</i></button>
                            <div class="modal" id="modal4">
                                <div class="modal-content">
                                  <h4>Delete Administrator</h4>
                                          <a>Apakah anda yakin untuk menghapus?</a>
                                          <div class="input-field">
                                            <!--  <input id="first_name" type="text" class="validate">-->
                                              <button class="waves-effect waves-light btn m-b-xs modal-trigger grey">Yes</button>
                                              <button class="waves-effect waves-light btn m-b-xs modal-trigger blue">No</button>
                                          </div>
                                </div>
                          </td>
                      </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
