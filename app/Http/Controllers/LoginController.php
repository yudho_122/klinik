<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;

class LoginController extends Controller
{
    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if($validator->fails()) {
            return redirect('login')->withErrors($validator)->withInput();
        }

        $email   =   $request->email;
        $password   =   $request->password;

        //$check  =   User::where('username', $username)->where('password', $password)->count();

        if( !($email == 'admin@gmail.com' && $password == 'admin') ) {
            return redirect('login')->with('status', 'salah');
        }

        // $take   =   User::where('username', $username)->where('password', $password)->first();

        session(['email' => 'admin@gmail.com']);
        session(['password' => 'admin']);

        return redirect('dashboard')->with(['status' => 'Login Berhasil, Selamat Datang admin']);
    }

    public function logout(Request $request) {
        $request->session()->regenerate();
        $request->session()->flush();

        return redirect('login');
    }
}
