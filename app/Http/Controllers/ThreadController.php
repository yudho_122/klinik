<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Thread_case;
use App\Thread_conversation;

class ThreadController extends Controller
{
    public function index() {

    }

    public function create(Request $request) {
    	$validator 	=	Validator::make($request->all(), [
    		'clinic_category_id' 	=>	'required',
    		'created_by' 			=> 	'required|max:45',
    		'detail' 				=>	'required',
    	]);

    	if($validator->fails()) {
            $error = $validator->messages()->toJson();

            echo json_encode(['status' => 'salah', 'message' => $error]);
        } else {
            $thread 						=	new Thread_case;
            $thread->thread_status 			=	'NEW';
            $thread->clinic_category_id		=	$request->input('clinic_category_id');
            $thread->created_by 			=	$request->input('created_by');
            $thread->detail 				=	$request->input('detail');
            $result 						= 	$thread->save();

            echo json_encode(['status' => 'success', 'message' => $request->all()]);
        }
    }

    public function aproveThread(Request $request) {
    	$thread_id 		=	$request->input('thread_case_id');
    	$thread 		=	Thread_case::find($thread_id);
    	$status 		=	$thread->thread_status;
    	if (!empty($status)) {
    		if ($status == 'OPEN') {
    			$thread 	=	Thread_case::where('thread_case_id', $thread_id)->update(['thread_status' => 'ON PROGRESS']);

    			echo json_encode(['status' => 'success', 'message' => 'Approve thread success']);
    		} else if($status == 'ON PROGRESS') {
    			$thread 	=	Thread_case::where('thread_case_id', $thread_id)->update(['thread_status' => 'COMPLETED']);

    			echo json_encode(['status' => 'success', 'message' => 'Completed thread success']);
    		}
    	} else {
    		echo json_encode(['status' => 'error', 'message' => 'Thread not found']);
    	}
    }

    public function rejectThread(Request $request) {
    	$thread_id 	=	$request->input('thread_case_id');
    	$thread 		=	Thread_case::find($thread_id);
    	$status 		=	$thread->thread_status;
    	$thread 	=	Thread_case::where('thread_case_id', $thread_id)->update(['thread_status' => 'REJECTED']);

    	echo json_encode(['status' => 'success', 'message' => 'Rejected success']);
    }

    public function commentThread(Request $request) {
    	$thread_id 	=	$request->input('thread_case_id');
    	$validator 	=	Validator::make($request->all(), [
    		'thread_case_id' 	=> 	'required',
    		'comments' 			=>	'required',
    		'created_by' 		=> 'required|max:45',
    	]);

    	if($validator->fails()) {
            $error = $validator->messages()->toJson();

            echo json_encode(['status' => 'salah', 'message' => $error]);
        } else {
        $comment 	=	New Thread_conversation;
	    	$comment->thread_case_id 	=	$thread_id;
	    	$comment->comments 			=	$request->input('comments');
	    	$comment->created_by 		=	$request->input('created_by');
	    	$comment->save();

            echo json_encode(['status' => 'success', 'message' => 'Comment success']);
        }
    }
}
