@extends('admintemplate.layout')

@section('content')
<br>

<div class="row" ng-app="clinicCategoryRecords" ng-controller="clinicCategoryController">
    <div class="col s12">
        <div class="page-title">Master Data</div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <span class="card-title">Clinic Category Data</span>
                <!-- <p>This is the news list that has been written all the time</p><br> -->
                <button class="waves-effect waves-light btn blue m-b-xs modal-trigger" href="#modal1" ng-click="toggle('add', 0)"><i class="material-icons left">add_box</i>Tambah</button>
                <table id="example" datatable="ng" class="display responsive-table datatable-example">
                    <thead>
                        <tr>
                            <th>Clinic Category Name</th>
                            <th>Clinic Category Des</th>
                            <th>Created at</th>
                            <th>Created by</th>
                            <th>Operation</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                          <th>Clinic Category Name</th>
                          <th>Clinic Category Des</th>
                          <th>Created at</th>
                          <th>Created by</th>
                          <th>Operation</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <tr ng-repeat="ca in clinic_category" id="text-tr">
                            <td>@{{ ca.clinic_category_name }}</td>
                            <td>@{{ ca.clinic_category_des }}</td>
                            <td>@{{ ca.created_at }}</td>
                            <td>@{{ ca.created_by }}</td>
                            <td style="width: 30%;">
                              <a class="btn indigo" style="bottom: 10px; right: 100px;" href="{{ url('/api/v1/clinic/view') }}/@{{ ca.clinic_category_id }}"><i class="material-icons">info</i></a>
                              <button id="modal-trigger-@{{ ca.clinic_category_id }}" class="waves-effect waves-light btn blue m-b-xs modal-trigger" style="bottom: 10px; left: 0.5px; top: 5px;" href="#modal@{{ ca.clinic_category_id }}" ng-click="toggle('edit', ca.clinic_category_id)" ng-mouseover="toggle('init', ca.clinic_category_id)"><i class="material-icons">mode_edit</i></button>
                              <button class="btn red" style="bottom: 10px; left: 200px;" ng-click="confirmDelete(ca.clinic_category_id)"><i class="material-icons">delete</i></button>

                              <script type="text/javascript">
                                //$('button[class*="modal-trigger"]').leanModal();
                              </script>

                              <!-- edit modal -->
                              <div id="modal@{{ ca.clinic_category_id }}" class="modal">
                                  <div class="modal-content">
                                      <h4>@{{form_title}}</h4>

                                        {{ csrf_field() }}
                                      <div class="input-field col s12">
                                        <input id="clinic_category_name" class="validate clinic_category_name" name="clinic_category_name" type="text" length="255"
                                        ng-model="clinic_category.clinic_category_name" value="@{{ca.clinic_category_name}}">
                                        <label for="clinic_category_name">Clinic Category Name</label>
                                      </div>

                                      <div class="input-field col s12">
                                          <div class="file-field input-field">
                                            <div class="btn white lighten-1">
                                              <span>File</span>
                                              <input type="file" class="validate" ng-files="setTheFiles($files)" id="clinic_category_image">
                                              </div>
                                            <div class="file-path-wrapper">
                                              <input class="file-path validate" type="text">
                                            </div>
                                          </div>
                                      </div>

                                      <div class="input-field col s12">
                                        <textarea id="clinic_category_des" name="clinic_category_des" class="materialize-textarea" ng-model="clinic_category.clinic_category_des"
                                        value="@{{ca.clinic_category_des}}"></textarea>
                                        <label for="clinic_category_des">Clinic Category Description</label>
                                      </div>

                                      <div class="input-field col s12">
                                        <input id="created_by" name="created_by" type="text" class="validate" length="255"
                                        ng-model="clinic_category.created_by" value="@{{ca.created_by}}">
                                        <label for="created_by" class="">Created By</label>
                                      </div>
                                  </div>

                                  <div class="modal-footer">
                                    <button type="button" class="modal-acion modal-close waves-effect waves-blue btn-flat" id="btn-save" ng-click="edit(modalstate, ca.clinic_category_id, clinic_category.clinic_category_name, clinic_category.clinic_category_des, clinic_category.created_by)">Save</button>
                                    <a href="#!" class="modal-acion modal-close waves-effect waves-blue btn-flat ">cancel</a>
                                  </div>
                              </div>
                              <!-- end edit modal -->
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- ini modal buat create -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4>@{{form_title}}</h4>
            <form>
              {{ csrf_field() }}
            <div class="input-field col s12">
              <input id="clinic_category_name" class="validate" name="clinic_category_name" type="text" length="255"
              ng-model="clinic_category.clinic_category_name">
              <label for="clinic_category_name">Clinic Category Name</label>
            </div>

            <div class="input-field col s12">
                <div class="file-field input-field">
                  <div class="btn white lighten-1">
                    <span>File</span>
                    <input type="file" class="validate" ng-files="setTheFiles($files)" id="clinic_category_image">
                    </div>
                  <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                  </div>
                </div>
            </div>

            <div class="input-field col s12">
              <textarea id="clinic_category_des" name="clinic_category_des" class="materialize-textarea" ng-model="clinic_category.clinic_category_des"></textarea>
              <label for="clinic_category_des">Clinic Category Description</label>
            </div>

            <div class="input-field col s12">
              <input id="created_by" name="created_by" type="text" class="validate" length="255"
              ng-model="clinic_category.created_by">
              <label class="">Created By</label>
            </div>
        </div>
      </form>
        <div class="modal-footer">
          <button type="button" class="modal-acion modal-close waves-effect waves-blue btn-flat" id="btn-save" ng-click="save(modalstate, clinic_category.clinic_category_id, clinic_category.clinic_category_name, clinic_category.clinic_category_des, clinic_category.created_by)">Save</button>
          <a href="#!" class="modal-acion modal-close waves-effect waves-blue btn-flat">cancel</a>
    </div>
</div>

@endsection
