var app = angular.module('sistemsetRecords', ['datatables'])
        .constant('API_URL', 'http://localhost/klinik-hutan/public/api/v1/');

app.controller('sistemsetsController', function($scope, $timeout ,$http, $window, API_URL) {
    //retrieve employees listing from API
    $window.location;

  //  $scope.groups=[];
    $http.get(API_URL + "sistem")
        .success(function(response) {
                $scope.sistem = response;
            });
            //show modal form
   $scope.toggle = function(modalstate, id) {
       $scope.modalstate = modalstate;

       switch (modalstate) {
           case 'add':
               $scope.sistem = "";
               $scope.form_title = "Add New Sistem Setting";
               break;
           case 'init':
               $('#modal-trigger-' + id).leanModal();
           break;
           case 'edit':
               $scope.form_title = "Sistem Setting Detail";
               $scope.system_setting_key = id;

               $http.get(API_URL + 'sistem/' + id)
                       .success(function(response) {
                           console.log(response);

                           $('label[for*="system_setting_key"], label[for*="system_setting_value"], label[for*="updated_by"]').addClass( 'active' );

                           $scope.sistem.system_setting_key   = response.system_setting_key;
                           $scope.sistem.system_setting_value = response.system_setting_value;
                           $scope.sistem.updated_by           = response.updated_by;

                       });
               break;
           default:
               break;
       }
  }

  var formData = new FormData();

  // save mode_edit
  $scope.edit = function(modalstate, id, system_setting_value, updated_by) {
      var url = API_URL + "sistem/" + id;

      formData.append('system_setting_key', id);
      formData.append('system_setting_value', system_setting_value);
      formData.append('updated_by', updated_by);

      //append employee id to the URL if the form is in edit mode
        $http({
          method: 'POST',
          url: url,
          data: formData,
          headers: {'Content-Type': undefined}
      }).success(function(response) {
          console.log(response);
          location.reload();
      }).error(function(response) {
          console.log(response);
          alert('This is embarassing. An error has occured. Please check the log for details');
      });
  }

   //save new record / update existing record
   $scope.save = function(modalstate, id, system_setting_value) {
       var url = API_URL + "sistem";

       formData.append('system_setting_key', id);
       formData.append('system_setting_value', system_setting_value);

       //append employee id to the URL if the form is in edit mode
         $http({
           method: 'POST',
           url: url,
           data: formData,
           headers: {'Content-Type': undefined}
       }).success(function(response) {
           console.log(response);
           location.reload();
       }).error(function(response) {
           console.log(response);
           alert('This is embarassing. An error has occured. Please check the log for details');
       });
   }

   //delete record
   $scope.confirmDelete = function(id) {
       var isConfirmDelete = confirm('Are you sure you want this record?');
       if (isConfirmDelete) {
           $http({
               method: 'DELETE',
               url: API_URL + 'sistem/' + id
           }).
                   success(function(data) {
                       console.log(data);
                       location.reload();
                   }).
                   error(function(data) {
                       console.log(data);
                       alert('Unable to delete');
                   });
       } else {
           return false;
       }
   }

});
