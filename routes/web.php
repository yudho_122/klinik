<?php

Route::get('/', function () {
    return view('login');
});

Route::get('/group', function () {
    return view('group.addgroup');
});

Route::get('/news', function () {
    return view('news.news');
});

Route::get('/resource', function () {
    return view('resource.index');
});

Route::get('/clinic', function () {
    return view('clinic_category.index');
});

Route::get('/thread', function () {
    return view('thread.index');
});

Route::get('/threadsingle', function () {
    return view('threadblueprint.Threadsingle');
});

Route::get('/threadlist', function () {
    return view('threadblueprint.Threadlist');
});

Route::get('/sis', function () {
    return view('system_setting.index');
});


Route::get('/report/re', function () {
    return view('report.resolution.index1');
});

Route::get('/user', function () {
    return view('user.daftaradmin');
});


Route::get('/re', function () {
    return view('enduser.resource.resourcelist');
});

 Route::post('thread/comment', 'ThreadController@commentThread')->name('commentThread');

Route::get('newslist', ['uses' => 'NewsEnduserController@getIndex', 'as' => 'enduser.newslist']);

Route::get('report/thread', ['uses' => 'ReportThreadController@reportThread', 'as' => 'report.thread.view_detil']);


//Route::get('resourcelist', ['uses' => 'ResourceenduserController@index', 'as' => 'enduser.resource.resourcelist']);

// Route::get('user/verify/{verification_code}', 'LoginController@verifyUser');
// Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
// Route::post('password/reset', 'Auth\PasswordController@reset');

Route::get('login', function() {
    return view('login');
});
Route::get('login', function() {
    return view('register');
});



Route::post('/report/resolution', 'ReportResolutionAnswerController@reportResolution')->name('reportResolution');
Route::post('resolution/pdf', 'ReportResolutionAnswerController@getPdf')->name('getPdf');
Route::post('report/pdf', 'ReportThreadController@downloadThread')->name('downloadThread');
Route::post('/report/thread', 'ReportThreadController@reportThread')->name('reportThread');


//Route::get('/report/pdf', 'ReportThreadController@downloadThread')->name('downloadThread');
Route::get('/api/v1/resourcelist', 'ResourceenduserController@index');


Route::get('/api/v1/user/{id?}', 'SystemAdminController@index')->name('user.index');
Route::get('/api/v1/user/edit/{id?}', 'SystemAdminController@edit')->name('user.edit');
Route::post('/api/v1/user', 'SystemAdminController@create');
Route::post('/api/v1/user/edit/{id?}', 'SystemAdminController@update')->name('user.update');
Route::post('/api/v1/user/getUser', 'SystemAdminControllers@getUser')->name('getUser');
Route::delete('/api/v1/user/delete/{id}', 'SystemAdminController@destroy');

Route::get('/api/v1/thread/create', 'threadCtrl@create');
Route::post('/api/v1/thread', 'threadCtrl@store');
Route::get('/api/v1/thread/{id?}', 'threadCtrl@index');
Route::delete('/api/v1/thread/{id}', 'threadCtrl@destroy');

Route::get('/api/v1/sistem/view/{id?}', 'SystemsettingController@tampil');
Route::get('/api/v1/sistem/{id?}', 'SystemsettingController@index');
Route::post('/api/v1/sistem', 'SystemsettingController@store');
Route::post('/api/v1/sistem/{id?}', 'SystemsettingController@update');
Route::delete('/api/v1/sistem/{id}', 'SystemsettingController@destroy');

Route::get('/api/v1/resource/view/{id?}', 'ResourceController@tampil');
Route::get('/api/v1/resource/{id?}', 'ResourceController@index');
Route::post('/api/v1/resource', 'ResourceController@store');
Route::post('/api/v1/resource/{id?}', 'ResourceController@update');
Route::post('resource/getresource', 'ResourceController@getresource');
Route::delete('/api/v1/resource/{id}', 'ResourceController@destroy');

Route::get('/api/v1/news/view/{id?}', 'NewsController@tampil');
Route::get('/api/v1/news/{id?}', 'NewsController@index')->name('news');
Route::post('/api/v1/news', 'NewsController@store');
Route::post('/api/v1/news/{id?}', 'NewsController@update');
Route::delete('/api/v1/news/{id}', 'NewsController@destroy');
Route::post('/api/v1/news/getnews', 'NewsController@getNews');

Route::get('/api/v1/group/{id?}', 'GroupController@index');
Route::post('/api/v1/group', 'GroupController@create');
Route::post('/api/v1/group/{id?}', 'GroupController@update');
Route::post('/api/v1/group/getgroup', 'GroupController@getGroup')->name('getGroup');
Route::delete('/api/v1/group/delete/{id}', 'GroupController@destroy');

Route::get('/api/v1/clinic/view/{id?}', 'ClinicCategoryController@tampil');
Route::get('/api/v1/clinic/{id?}', 'ClinicCategoryController@index');
Route::post('/api/v1/clinic', 'ClinicCategoryController@store');
Route::post('/api/v1/clinic/{id?}', 'ClinicCategoryController@update');
Route::post('/api/v1/clinic/getgroup', 'ClinicCategoryController@getGroup');
Route::delete('/api/v1/clinic/{id}', 'ClinicCategoryController@destroy');

Route::get('logout', 'LoginController@logout')->name('logout');
Route::post('login', 'LoginController@login')->name('login');
Route::post('register', 'RegisterController@register')->name('register');


// Route::group(['middleware' => 'Login'], function() {
//     Route::get('dashboard', 'DashboardController@index')->name('dashboard');
//     Route::get('news', 'NewsController@index')->name('news');
//
//     // CRUD NEWS
//     Route::post('news/create', 'NewsController@create')->name('createNews');
//     Route::post('news/update', 'NewsController@update')->name('updateNews');
//     Route::post('news/getnews', 'NewsController@getNews')->name('getNews');
//     Route::delete('news/delete/{id}', 'NewsController@destroy');
//
//     // CRUD GROUP
//     Route::post('group/create', 'GroupController@create')->name('createGroup');
//     Route::post('group/update', 'GroupController@update')->name('updateGroup');
//     Route::post('group/getgroup', 'GroupController@getGroup')->name('getGroup');
//     Route::delete('group/delete/{id}', 'GroupController@destroy');
//
//     // CRUD RESOURCE
//     Route::post('resource/create', 'ResourceController@create')->name('createResource');
//     Route::post('resource/update', 'ResourceController@update')->name('updateResource');
//     Route::post('resource/getresource', 'ResourceController@getresource')->name('getResource');
//     Route::delete('resource/delete/{id}', 'ResourceController@destroy');
//
//     // CRUD CLINIC CATAGORY
//     Route::post('clinic/create', 'ClinicCategoryController@create')->name('createClinic');
//     Route::post('clinic/update', 'ClinicCategoryController@update')->name('updateClinic');
//     Route::post('clinic/getclinic', 'ClinicCategoryController@getClinic')->name('getClinic');
//     Route::delete('clinic/delete/{id}', 'ClinicCategoryController@destroy');
//
//     // CRUD THREAD
//     Route::post('thread/create', 'ThreadController@create')->name('createThread');
//     Route::post('thread/aprove', 'ThreadController@aproveThread')->name('aproveThread');
//     Route::post('thread/reject', 'ThreadController@rejectThread')->name('rejectThread');
//     Route::post('thread/comment', 'ThreadController@commentThread')->name('commentThread');
// });
