<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContentManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('content_managements', function (Blueprint $table) {
            $table->increments('content_management_id');
            $table->string('content_management_type', 45)->nullable();
            $table->text('content_management_path')->nullable();
            $table->string('content_management_lat', 45)->nullable();
            $table->string('content_management_long', 45)->nullable();
            $table->integer('thread_case_id')->unsigned();
            $table->foreign('thread_case_id')->references('thread_case_id')->on('thread_cases');
            $table->string('content_management_original_name', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('content_managements');
    }
}
