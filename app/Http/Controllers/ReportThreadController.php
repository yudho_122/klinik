<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Clinic_category;
use PDF;

class ReportThreadController extends Controller
{
    public function index() {

    }

    public function reportThread(Request $request) {

      $clinic_categorys = Clinic_category::all();
      $clinic = $request->input('clinic');
      $start = $request->input('dateAwal');
      $end = $request->input('dateAkhir');
      $awal 	=  Carbon::parse($start)->format('Y-m-d');
      $akhir 	=	 Carbon::parse($end)->format('Y-m-d');

      if($clinic == 'all'){

    	$report 	=	DB::table('thread_cases')
    						->join('clinic_categorys', 'thread_cases.clinic_category_id', '=', 'clinic_categorys.clinic_category_id')
    						->join('thread_conversations', 'thread_cases.thread_case_id', '=', 'thread_conversations.thread_case_id')
    						->select('thread_cases.subject', 'clinic_categorys.clinic_category_name', 'thread_cases.created_by', 'thread_cases.created_at',(DB::raw('count(*) as comment')), 'thread_cases.thread_status')
    						->groupBy('thread_conversations.thread_case_id')
            		->whereBetween('thread_cases.created_at', [$awal, $akhir])
    						->get();
        }else{
          $report 	=	DB::table('thread_cases')
        						->join('clinic_categorys', 'thread_cases.clinic_category_id', '=', 'clinic_categorys.clinic_category_id')
        						->join('thread_conversations', 'thread_cases.thread_case_id', '=', 'thread_conversations.thread_case_id')
        						->select('thread_cases.subject', 'clinic_categorys.clinic_category_name', 'thread_cases.created_by', 'thread_cases.created_at',(DB::raw('count(*) as comment')), 'thread_cases.thread_status')
        						->groupBy('thread_conversations.thread_case_id')
                    ->where('thread_cases.clinic_category_id', '=', $clinic)
        						->whereBetween('thread_cases.created_at', [$awal, $akhir])
        						->get();
        }

        return view('report.thread.view_detil', compact('report', 'start', 'end', 'clinic_categorys'));
        //return response()->json(['hasil' => $report]);

      //return view('report.pdfthread', compact('start', 'end', 'report'));

    }

    public function downloadThread(Request $request){
      $start = $request->input('dateAwal');
      $end = $request->input('dateAkhir');
      $awal 	=  Carbon::parse($start)->format('Y-m-d');
      $akhir 	=	 Carbon::parse($end)->format('Y-m-d');
    	$report 	=	DB::table('thread_cases')
    						->join('clinic_categorys', 'thread_cases.clinic_category_id', '=', 'clinic_categorys.clinic_category_id')
    						->join('thread_conversations', 'thread_cases.thread_case_id', '=', 'thread_conversations.thread_case_id')
    						->select('thread_cases.subject', 'clinic_categorys.clinic_category_name', 'thread_cases.created_by', 'thread_cases.created_at',(DB::raw('count(*) as comment')), 'thread_cases.thread_status')
    						->groupBy('thread_conversations.thread_case_id')
    						->whereBetween('thread_cases.created_at', [$awal, $akhir])
    						->get();

      //$$this->reportThread($request);
      //return view('report.pdfthread', compact('start', 'end', 'report'));
      $pdf = PDF::loadView('report.thread.pdf', compact('report', 'start', 'end'));
      return $pdf->download('Thread-'.$start.'-'.$end.'-Report.pdf');
    }
}
