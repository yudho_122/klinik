<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ThreadConvTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('thread_conversations', function (Blueprint $table) {
          $table->bigIncrements('thread_conversation_id');
          $table->integer('thread_case_id')->unsigned();
          $table->foreign('thread_case_id')->references('thread_case_id')->on('thread_cases');
          $table->text('comments');
          $table->dateTime('created_at');
          $table->string('created_by',45);
          $table->integer('content_management_id')->nullable();
          $table->text('message_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('thread_conversations');
    }
}
