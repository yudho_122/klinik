@extends('admintemplate.layout')

@section('content')

	<div class="row" ng-app="newsRecords" ng-controller="newssController">
		<div class="col s8">
			<h1>{{$news->news_title}}</h1>
			<p><img src="#" style="max-height:100px;max-width:100px;margin-top:10px;"></p>
			<p class="lead">{{$news->news_content}}</p>
		</div>

      <div class="col s4">
        <div class="card white">
          <dl class="card-content">
            <label>Url:</label>
              <p><a href="#">ini url Thread</a></p>
          </dl>
          <dl class="card-content">
            <label>Created At:</label>
            <p>{{$news->created_at}}</p>
          </dl>

          <dl class="card-content">
            <label>Last Updated:</label>
            <p>{{$news->updated_at}}</p>
          </dl>
          <dl class="card-content">
            <label>Created By:</label>
            <p>{{$news->created_by}}</p>
          </dl>

        <hr>
          <div class="row">
            <div class="col s6">
              <button id="modal-trigger-1" class="waves-effect waves-light btn greeb m-b-xs modal-trigger" style="width: 130px;" href="#modal1" ng-click="toggle('edit', {{$news->news_id}})" ng-mouseover="toggle('init', newz.news_id)">Edit</button>
							<div id="modal1" class="modal">
									<div class="modal-content">
											<h4>@{{form_title}}</h4>

												{{ csrf_field() }}
											<div class="input-field col s12">
												<input id="news_title" class="validate news_title" name="news_title" type="text" length="255"
												ng-model="news.news_title" value="@{{newz.news_title}}">
												<label for="news_title">Title</label>
											</div>

											<div class="input-field col s12">
													<div class="file-field input-field">
														<div class="btn white lighten-1">
															<span>File</span>
															<input type="file" class="validate" ng-files="setTheFiles($files)" id="news_picture_path">
															</div>
														<div class="file-path-wrapper">
															<input class="file-path validate" type="text" value="@{{newz.news_picture_path}}">
														</div>
													</div>
											</div>

											<div class="input-field col s12">
												<textarea id="news_content" name="news_content" class="materialize-textarea" ng-model="news.news_content"
												value="@{{newz.news_content}}"></textarea>
												<label for="news_content">News Content</label>
											</div>

											<div class="input-field col s12">
												<input id="created_by" name="created_by" type="text" class="validate" length="255"
												ng-model="news.created_by" value="@{{newz.created_by}}">
												<label for="created_by" class="">created_by</label>
											</div>
									</div>

									<div class="modal-footer">
											<button type="button" class="modal-action modal-close waves-effect waves-blue btn-flat " id="btn-save" ng-click="edit(modalstate, {{$news->news_id}}, news.news_title, news.news_content, news.created_by)">Save</button>
											<a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">cancel</a>
									</div>
							</div>
          </div>

          <div class="col s6">
            <!--<a href="#" class="waves-effect waves-light btn red m-b-xs" style="width: 150px;">Delete</a>-->
            <button class="waves-effect waves-light btn m-b-xs modal-trigger red" ng-click="confirmDelete({{$news->news_id}})" style="width: 130px;">Delete</button>
                <div class="modal" id="modal3">
                    <div class="modal-content">
                      <h4>Delete Group</h4>
                              <a>Apakah anda yakin untuk menghapus?</a>
                              <div class="input-field">
                                <!--  <input id="first_name" type="text" class="validate">-->
                                  <button class="waves-effect waves-light btn m-b-xs modal-trigger">Yes</button>
                                  <button class="waves-effect waves-light btn m-b-xs modal-trigger">No</button>
                              </div>
                    </div>
              </div>
          </div>
          <div class="col s12 m6 l8">
            <a href="#" class="waves-effect waves-light btn blue m-b-xs" ng-click="redirectNews()" style="width: 285px;">See All News</a>
          </div>
        </div>
      </div>
      </div>
    </div>

@endsection
