@extends('admintemplate.layout')

@section('content')
<div class="row">
    <div class="col s12">
        <div class="page-title"></div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <span class="card-title">Resource List</span>
                <table id="example" class="display responsive-table datatable-example">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>No. Telp</th>
                            <th>E-mail</th>
                            <th>Created at</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                          <th>Nama</th>
                          <th>Alamat</th>
                          <th>No. Telp</th>
                          <th>E-mail</th>
                          <th>Created at</th>
                          <th>Status</th>
                          <th></th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <tr>
                            <td>Dora the explorer</td>
                            <td>Jl. Candi Panggung</td>
                            <td>01222</td>
                            <td>dora@yahoo.com</td>
                            <td>1/26/2018</td>
                            <td>
                                <div class="collection-item">
                                  <span class="new badge red" data-badge-caption="need approvement" style="right: 285px; top: 220px;"></span>
                                </div>
                            </td>
                            <td><a href="#" class="waves-effect waves-light btn blue m-b-xs" style="left: 30px;">Approve</a> <a href="#" class="waves-effect waves-light btn red m-b-xs" style="left: 30px;">Reject</a></td>
                        </tr>
                        <tr>
                            <td>Boots si monyet</td>
                            <td>Sebelah rumah dora</td>
                            <td>013122</td>
                            <td>Boots@gugel.com</td>
                            <td>2/26/2018</td>
                            <td>
                                <div class="collection-item">
                                  <span class="new badge blue" data-badge-caption="Approved" style="right: 310px; top: 140px;"></span>
                                </div>
                            </td>
                            <td><a href="#" class="waves-effect waves-light btn blue m-b-xs" style="left: 30px;">Approve</a> <a href="#" class="waves-effect waves-light btn red m-b-xs" style="left: 30px;">Reject</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
