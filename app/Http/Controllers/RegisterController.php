<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;
use Hash, Mail;
class RegisterController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name'                =>  'required|max:45',
            'last_name'                 =>  'required|max:45',
            'email'                     =>  'required|email|max:45|unique:users',
            'password'                  =>  'required|confirmed|min:6',
            'password_confirmation'     =>  'required',
            'group_id'                  =>  'required',
            'telp'                      =>  'required',

        ]);

        if($validator->fails()) {
            $error = $validator->messages()->toJson();

            echo json_encode(['status' => 'salah', 'message' => $error]);
        } else {
            // $user   =   User::create([
            //     'first_name'    =>  $request->input('first_name'),
            //     'last_name'     =>  $request->input('last_name'),
            //     'email'         =>  $request->input('email'),
            //     'password'      =>  Hash::make($request->input('password')),
            //     'group_id'      =>  $request->input('group_id'),
            //     'telp'          =>  $request->input('telp'),
            //     'user_approval_status'  =>  'WAITING'
            // ]);
            $email  =   $request->input('email');
            $first_name     =   $request->input('first_name');
            $verification_code = str_random(30);
            $subject = "Please verify your email address.";
            $data   =   ['first_name' => $first_name, 'verification_code' => $verification_code];

            Mail::send('email.verify', $data, function ($message) use($email, $first_name, $subject) {
                $message->from('akunbitcoiner@gmail.com', 'Learning Laravel');
                $message->to($email, $first_name);
                $message->subject($subject);

            });
            return response()->json(['success'=> true, 'message'=> 'Thanks for signing up! Please check your email to complete your registration.']);
            // echo json_encode(['status' => 'success', 'message' => $user]);
        }
    }

    public function verifyUser($verification_code)
    {
        $check = DB::table('user_verifications')->where('token',$verification_code)->first();

        if(!is_null($check)){
            $user = Sys_admin::find($check->user_id);

            if($user->is_verified == 1){
                return response()->json([
                    'success'=> true,
                    'message'=> 'Account already verified..'
                ]);
            }

            // $user->update(['is_verified' => 1]);
            DB::table('user_verifications')->where('token',$verification_code)->delete();

            return response()->json([
                'success'=> true,
                'message'=> 'You have successfully verified your email address.'
            ]);
        }

        return response()->json(['success'=> false, 'error'=> "Verification code is invalid."]);

    }


    public function login(Request $request)
    {
        $rules = [
            'email' => 'required|email',
            'password' => 'required',
        ];

        $input = $request->only('email', 'password');

        $validator = Validator::make($input, $rules);

        if($validator->fails()) {
            $error = $validator->messages()->toJson();
            return response()->json(['success'=> false, 'error'=> $error]);
        }

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['success' => false, 'error' => 'Invalid Credentials. Please make sure you entered the right information and you have verified your email address.'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json(['success' => true, 'data'=> [ 'token' => $token ]]);

        //return redirect('dashboard')->with(['success' => true, 'data'=> [ 'token' => $token ]]);
    }


    /**
     * Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to relogin to get a new token
     *
     * @param Request $request
     */
    public function logout(Request $request) {
        $this->validate($request, ['token' => 'required']);

        try {
            JWTAuth::invalidate($request->input('token'));
            return response()->json(['success' => true]);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
        }
    }


    /**
     * API Recover Password
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    // public function recover(Request $request)
    // {
    //     $user = User::where('email', $request->email)->first();
    //     if (!$user) {
    //         $error_message = "Your email address was not found.";
    //         return response()->json(['success' => false, 'error' => ['email'=> $error_message]], 401);
    //     }

    //     try {
    //         Password::sendResetLink($request->only('email'), function (Message $message) {
    //             $message->subject('Your Password Reset Link');
    //         });

    //     } catch (\Exception $e) {
    //         //Return with error
    //         $error_message = $e->getMessage();
    //         return response()->json(['success' => false, 'error' => $error_message], 401);
    //     }

    //     return response()->json([
    //         'success' => true, 'data'=> ['msg'=> 'A reset email has been sent! Please check your email.']
    //     ]);
    // }

    public function getAuthUser(Request $request){
        $user = JWTAuth::toUser($request->token);
        return response()->json(['result' => $user]);
    }
}
